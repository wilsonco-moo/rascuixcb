This is part of the [rascUI project](https://gitlab.com/wilsonco-moo/rascui).

# rascUIxcb

This project is an implementation of the rascUI platform system, using the
[XCB library](https://xcb.freedesktop.org/). A platform implementation provides
all of the platform-specific stuff, in order for a rascUI based graphical
application to run within a particular platform (in this case desktop Linux,
using the X Window System). See
[rascUIwin](https://gitlab.com/wilsonco-moo/rascuiwin) for a platform
implementation for use within desktop Windows.

The [rascUItheme](https://gitlab.com/wilsonco-moo/rascuitheme) project contains
some UI themes for use with rascUIxcb, in
[src/rascUItheme/themes/xcb](https://gitlab.com/wilsonco-moo/rascuitheme/-/tree/master/src/rascUItheme/themes/xcb).

For information about how to use rascUI (with or without a platform
implementation), see the
[rascUI project](https://gitlab.com/wilsonco-moo/rascui). For an example program
which uses rascUI to provide a cross-platform user interface, see the
[map naming program](https://gitlab.com/wilsonco-moo/mapnamer).

# Screenshots

Here is an example of the rascUI demo program, running using the rascUIxcb
platform system, using the scalable theme (from rascUItheme), within desktop
Linux.

![Image](development/screenshots/desktopLinux.png)

Here is an example of the
[map naming program](https://gitlab.com/wilsonco-moo/mapnamer), running using
the rascUIxcb platform system, using the scalable theme (from rascUItheme),
within desktop Linux.

![Image](development/screenshots/mapnamer.png)
