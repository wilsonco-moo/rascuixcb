/*
 * Context.cpp
 *
 *  Created on: 15 May 2020
 *      Author: wilson
 */

#include "Context.h"

#include <rascUItheme/utils/config/ConfigFile.h>
#include <rascUItheme/utils/config/PathUtils.h>
#include <rascUI/util/RepaintController.h>
#include <rascUI/util/Bindings.h>
#include <rascUI/base/Theme.h>
#include <xcb/xcb_util.h>
#include <xcb/xcb_aux.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <chrono>

#include "../util/Misc.h"
#include "Window.h"

namespace rascUIxcb {
    
    // Used by the two macros below.
    #define GET_WINDOW_FROM_ID_OR_BREAK_INTERNAL(windowId, varName, warning)                                    \
        Window * varName;                                                                                       \
        {                                                                                                       \
            auto iter = windowsById.find(windowId);                                                             \
            if (iter == windowsById.end()) {                                                                    \
                warning                                                                                         \
                break;                                                                                          \
            }                                                                                                   \
            varName = iter->second;                                                                             \
        }
    
    /**
     * Creates a variable called "varName", and stores the window in it. If accessing the
     * window failed, a warning is printed and "break" is run.
     */
    #define GET_WINDOW_FROM_ID_OR_BREAK(windowId, eventName, varName)                                           \
        GET_WINDOW_FROM_ID_OR_BREAK_INTERNAL(windowId, varName,                                                 \
            std::cerr << "WARNING: rascUIxcb::Context: " << eventName << " event from unknown window.\n";)
    
    /**
     * Same as above, but no warning is printed for unknown windows.
     */
    #define GET_WINDOW_FROM_ID_OR_BREAK_NO_WARNING(windowId, varName) \
        GET_WINDOW_FROM_ID_OR_BREAK_INTERNAL(windowId, varName, )
        
    
    const char * const Context::CONFIG_FILENAME = "rascUIxcb.cfg";
    
    // If a custom theme path has been provided by project
    // configuration, use it for the theme directory. Otherwise fall
    // back to working directory (see comment in header).
    #ifdef RASCUIXCB_THEME_PATH
        const char * const Context::THEME_DIRECTORY = RASCUIXCB_THEME_PATH;
    #else
        const char * const Context::THEME_DIRECTORY = "";
    #endif
    
    Context::Context(rascUI::Theme * defaultTheme, const GLfloat * defaultXScalePtr, const GLfloat * defaultYScalePtr, const rascUI::Bindings * defaultBindings, const char * displayname) :
        
        // Note that we ALWAYS enable partial repaint for xcb.
        rascUI::ContextBase(defaultTheme,
                            (defaultBindings == NULL) ? &rascUI::Bindings::defaultX11 : defaultBindings,
                            defaultXScalePtr, defaultYScalePtr, true),
        
        // Setup the X connection, screen, key symbols table and atoms.
        // Note: Connection can be NULL if it failed!
        connection(displayname),
        screen(xcbGetConnection() ? xcb_setup_roots_iterator(xcb_get_setup(xcbGetConnection())).data : NULL),
        keySymbols(connection.getConnection()),
        screenConfig(*this),
        atomWmProtocols(connection.getConnection(), false, 12, "WM_PROTOCOLS"),
        atomWmDeleteWindow(connection.getConnection(), false, 16, "WM_DELETE_WINDOW"),
        doubleBufferEnabled(true),
        drawWindowBackgrounds(false),
        autoConstructFailed(false) {
        
        // Common initialisation stuff.
        xcbInitialise();
    }
    
    Context::Context(bool runInitialise, const char * displayname) :
        rascUI::ContextBase(),
        
        // Setup the X connection, screen and key symbols table.
        // Note: Connection can be NULL if it failed!
        connection(displayname),
        screen(xcbGetConnection() ? xcb_setup_roots_iterator(xcb_get_setup(xcbGetConnection())).data : NULL),
        keySymbols(connection.getConnection()),
        screenConfig(*this),
        atomWmProtocols(connection.getConnection(), false, 12, "WM_PROTOCOLS"),
        atomWmDeleteWindow(connection.getConnection(), false, 16, "WM_DELETE_WINDOW"),
        doubleBufferEnabled(true),
        drawWindowBackgrounds(false),
        autoConstructFailed(false) {
        
        if (wasSuccessful()) {
            // Before we block waiting for a config file to load, flush (for the
            // atom requests). We want to wait for the x server to respond while
            // we're loading the config file, not flush afterwards!
            xcb_flush(xcbGetConnection());
            
            // If we loaded successfully, load config. If config fails to load, set default config and save.
            rascUItheme::ConfigFile config((rascUItheme::PathUtils::getRascUIConfigDirectory() + CONFIG_FILENAME).c_str());
            if (!config.loadedSuccessfully()) {
                config.setValue("theme", "libscalableTheme.so");
                config.setValue("uiScaleExp", "0");
                config.setValue("doubleBufferEnabled", "1");
                config.setValue("drawWindowBackgrounds", "0");
                config.save();
            }
            
            // Since screen config needs two round-trips, get it to receive then
            // send out the next bit here. Again, flush so we can wait for the
            // x server to respond while we wait for the theme to load.
            screenConfig.requestCrtcs();
            xcb_flush(xcbGetConnection());
            
            // Get theme filename and load it: extract only the base filename part of the filename in the
            // config file, to ensure a file within THEME_DIRECTORY is used.
            const std::string themeFilename = THEME_DIRECTORY + rascUItheme::PathUtils::getFilename(config.getValue("theme"));
            autoConstructThemeLoader = rascUItheme::DynamicThemeLoader(themeFilename);
            if (autoConstructThemeLoader.loadedSuccessfully()) {
                
                // If theme loaded successfully, get the theme and set the remainder of the ContextBase config.
                // Always use X11 bindings, convert exp scale, use same scale for X and Y, and ALWAYS use partial repaint.
                defaultTheme = autoConstructThemeLoader.getTheme();
                defaultBindings = &rascUI::Bindings::defaultX11;
                autoConstructUiScale = scaleExpToScale(std::strtof(config.getValue("uiScaleExp"), NULL));
                defaultXScalePtr = &autoConstructUiScale;
                defaultYScalePtr = &autoConstructUiScale;
                doubleBufferEnabled = (std::strcmp(config.getValue("doubleBufferEnabled"), "1") == 0);
                drawWindowBackgrounds = (std::strcmp(config.getValue("drawWindowBackgrounds"), "1") == 0);
                allowPartialRepaint = true;
                
            } else {
                // Complain and set fail flag if theme failed to load.
                std::cerr << "WARNING: rascUIxcb::Context: " << "Failed to load theme from file: \"" << themeFilename << "\".\n";
                autoConstructFailed = true;
            }
        }
        
        // Common initialisation stuff, (if needed by the bool flag).
        if (runInitialise) {
            xcbInitialise();
        }
    }
    
    Context::~Context(void) {
        // Call the Theme's destroy method BEFORE we free the platform stuff, (if we have a Theme).
        if (defaultTheme != NULL) {
            defaultTheme->callDestroy();
        }
    }
    
    void Context::registerWindow(Window * window) {
        windowsById[window->xcbGetWindow()] = window;
    }

    void Context::unregisterWindow(Window * window) {
        windowsById.erase(window->xcbGetWindow());
    }
    
    bool Context::wasSuccessful(void) const {
        return connection.connectionSuccessful() && screen != NULL && keySymbols.wasSuccessful() && !autoConstructFailed;
    }
    
    void Context::flush(void) {
        xcb_flush(xcbGetConnection());
    }
        
    unsigned long long Context::mainLoop(void) {
        // Create resources at the start of the loop, start counting events.
        xcbMainLoopInit();
        unsigned long long eventCount = 0;
        
        // Loop until it is requested that we stop (from endMainLoop), or we have run out of (mapped) windows.
        while(continueMainLoop && xcbCountMappedWindows() > 0) {
            
            // Get event (note that we must free this).
            xcb_generic_event_t * event;
            bool error;
            xcbMainLoopGetEvent(&event, &error);
            
            // If there was an error in getting the event, end the main loop (but free the event anyway!). Otherwise, increment the event count.
            if (error) {
                free(event);
                break;
            }
            eventCount++;
            
            // If an event was created handle the event.
            bool repaintNeeded = true;
            if (event != NULL) {
                xcbMainLoopHandleEvent(event, &repaintNeeded);
            }
            
            // Repaint and sync if needed, even if there was no event (we still need to update function queues in that case).
            if (repaintNeeded) {
                xcbMainLoopRepaintAndSync();
            }
            
            // Free the event since it is dynamically allocated. When we have no event, it will be NULL (but freeing NULL is fine).
            free(event);
        }
        
        // Clean up at end, return event count.
        xcbMainLoopCleanUp();
        return eventCount;
    }

    void Context::endMainLoop(void) {
        continueMainLoop = false;
    }
    
    void Context::repaintEverything(void) {
        for (const std::pair<const xcb_window_t, Window *> & pair : windowsById) {
            if (pair.second->isMapped()) {
                pair.second->repaint();
            }
        }
    }
    
    size_t Context::xcbCountMappedWindows(void) const {
        size_t count = 0;
        for (const std::pair<const xcb_window_t, Window *> & pair : windowsById) {
            count += pair.second->isMapped();
        }
        return count;
    }
    
    Window * Context::xcbGetWindowById(const xcb_window_t windowId) const {
        const std::unordered_map<xcb_window_t, Window *>::const_iterator iter = windowsById.find(windowId);
        if (iter != windowsById.end()) {
            return iter->second;
        }
        return NULL;
    }
    
    void Context::setRootCursor(xcb_cursor_t rootCursor) {
    }
    
    void Context::xcbInitialise(void) {
        // Note: Explicitly call OUR implementation of wasSuccessful here.
        // Subclasses can override that and call xcbInitialise in THEIR
        // constructor - THEIR implementation might return nonsense since
        // they might not have done initialising yet!
        bool successful = Context::wasSuccessful();
        
        // Only call the Theme's init method if loading the platform was successful, and if
        // we actually have a Theme. Note that the user data is a Context: this.
        if (defaultTheme != NULL && successful) {
            defaultTheme->callInit(this);
        }
        
        // Complain if connecting to the X server failed.
        if (!successful) {
            std::cerr << "WARNING: rascUIxcb::Context: " << "Failed to connect to X server.\n";
        }
    }
    
    void Context::xcbMainLoopInit(void) {
        // Ignore any calls to endMainLoop which happened outside mainLoop, and automatically flush the connection.
        continueMainLoop = true;
        xcb_flush(xcbGetConnection());
        
        // Tell the function queues to use the connection's pipe. Outside the main loop, there is no point in the
        // function queues using the pipe. Also initially update them, before we start waiting around.
        beforeEvent.setUpdateFileDescriptor(connection.getWritePipe());
        afterEvent.setUpdateFileDescriptor(connection.getWritePipe());
        beforeEvent.update();
        afterEvent.update();
    }

    void Context::xcbMainLoopGetEvent(xcb_generic_event_t ** event, bool * error) {
        // Ask the function queues how long until their next timed event.
        unsigned long long timeNow = (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch())).count(),
                           minTimeUntil = (unsigned long long)-1;
        bool hasWaiting = false;
        beforeEvent.hasWaitingTimedFunctions(timeNow, &minTimeUntil, &hasWaiting);
        afterEvent.hasWaitingTimedFunctions(timeNow, &minTimeUntil, &hasWaiting);
        
        // Get a piped event, using the minimum timeout from the function queues.
        connection.getPipedEvent(event, error, hasWaiting ? &minTimeUntil : NULL);
        
        // The before event function queue ALWAYS gets processed before the event, even if there is an error.
        beforeEvent.update();
    }

    void Context::xcbMainLoopHandleEvent(xcb_generic_event_t * event, bool * repaintNeeded) {
        // Decide what to do based on the event type.
        // Get event type using macro from xcb util (xcb_event.h), see
        // http://web.archive.org/web/20101224073645/http://xcb.freedesktop.org/XcbUtil/api/group__xcb____event__t.html
        const uint8_t eventResponseType = XCB_EVENT_RESPONSE_TYPE(event);
        switch(eventResponseType) {

        case XCB_EXPOSE: {
            xcb_expose_event_t * exposeEvent = (xcb_expose_event_t *)event;
            
            // Do nothing if the next event is also an expose from the same
            // window - don't care, we can just skip this one.
            // (Even if we did try to repaint here, we wouldn't actually *do*
            // the repaint until no more events were waiting).
            if (RASCUIXCB_NEXT_EVENT_IS_WINDOW(connection, XCB_EXPOSE, xcb_expose_event_t, window, exposeEvent->window)) {
                *repaintNeeded = false;
                
            // All we need to do with expose events is update the window geometry and request a repaint of everything (if the window exists).
            } else {
                GET_WINDOW_FROM_ID_OR_BREAK(exposeEvent->window, "expose", window)
                // Tell the window that it has received its first expose event - to allow it to repaint.
                window->xcbOnReceiveExposeEvent();
                window->repaint();
            }
            break;
        }
        
        case XCB_CONFIGURE_NOTIFY: {
            xcb_configure_notify_event_t * const configureEvent = (xcb_configure_notify_event_t *)event;
            
            // Do nothing if the next event is also a configure notify for the
            // same window - don't care, we can just skip this one.
            if (RASCUIXCB_NEXT_EVENT_IS_WINDOW(connection, XCB_CONFIGURE_NOTIFY, xcb_configure_notify_event_t, window, configureEvent->window)) {
                *repaintNeeded = false;
            
            // All we need to do with configure notify events is update the
            // window geometry and request a repaint of everything (if the
            // window exists).
            } else {
                GET_WINDOW_FROM_ID_OR_BREAK_NO_WARNING(configureEvent->window, window)
                
                // Note: Window draw area keeps x and y of zero!
                // Also take note if we resized.
                rascUI::Rectangle & drawArea = window->xcbGetWindowDrawArea();
                const bool hasResized = (drawArea.width != configureEvent->width || drawArea.height != configureEvent->height);
                drawArea.width = configureEvent->width;
                drawArea.height = configureEvent->height;
                
                
                rascUI::Rectangle & geometryArea = window->xcbGetWindowGeometryArea();
                geometryArea.x = configureEvent->x;
                geometryArea.y = configureEvent->y;
                geometryArea.width = configureEvent->width;
                geometryArea.height = configureEvent->height;
                
                // Tell the whole window that it needs repainting, if it has
                // been resized. It won't actually *do* the repaint until no
                // more events are waiting (i.e: If we're sent loads of configure
                // notifies and probably lots of exposes, during a window resize).
                if (hasResized) {
                    window->repaint();
                }
            }
            break;
        }
        
        // Note: Treat window enter notify events exactly the same as normal mouse motion events.
        case XCB_ENTER_NOTIFY: case XCB_MOTION_NOTIFY: {
            xcb_motion_notify_event_t * motionEvent = (xcb_motion_notify_event_t *)event;
            
            // PURPOSEFULLY IGNORE mouse motion events if there is another one after it.
            // On a slower system, this removes the irritating lag-behind, especially noticeable when scrolling.
            // In essence, if there are more mouse motion events than we can draw, we ignore some to avoid becoming
            // too far behind.
            if (RASCUIXCB_NEXT_EVENT_IS_WINDOW(connection, XCB_MOTION_NOTIFY, xcb_motion_notify_event_t, event, motionEvent->event)) {
                *repaintNeeded = false;
            } else {
                GET_WINDOW_FROM_ID_OR_BREAK(motionEvent->event, "motionNotify", window)
                if (window->mouseMove(motionEvent->event_x, motionEvent->event_y)) {
                    window->onUnusedMouseMove(motionEvent->event_x, motionEvent->event_y);
                }
            }
            break;
        }
        
        case XCB_BUTTON_PRESS: {
            xcb_button_press_event_t * pressEvent = (xcb_button_press_event_t *)event;
            GET_WINDOW_FROM_ID_OR_BREAK(pressEvent->event, "button press", window)
            if (window->mouseEvent(pressEvent->detail, rascUI::MouseEvents::PRESS, pressEvent->event_x, pressEvent->event_y)) {
                window->onUnusedMouseEvent(pressEvent->detail, rascUI::MouseEvents::PRESS, pressEvent->event_x, pressEvent->event_y);
            }
            break;
        }

        case XCB_BUTTON_RELEASE: {
            xcb_button_release_event_t * releaseEvent = (xcb_button_release_event_t *)event;
            GET_WINDOW_FROM_ID_OR_BREAK(releaseEvent->event, "button release", window)
            if (window->mouseEvent(releaseEvent->detail, rascUI::MouseEvents::RELEASE, releaseEvent->event_x, releaseEvent->event_y)) {
                window->onUnusedMouseEvent(releaseEvent->detail, rascUI::MouseEvents::RELEASE, releaseEvent->event_x, releaseEvent->event_y);
            }
            break;
        }
        
        case XCB_KEY_PRESS: {
            xcb_key_press_event_t * pressEvent = (xcb_key_press_event_t *)event;
            xcb_keysym_t key = keySymbols.keyCodeToKeySym(pressEvent->detail, pressEvent->state);
            GET_WINDOW_FROM_ID_OR_BREAK(pressEvent->event, "key press", window)
            if (window->keyPress((int)key, false)) {
                window->onUnusedKeyPress((int)key, false);
            }
            break;
        }

        case XCB_KEY_RELEASE: {
            xcb_key_release_event_t * releaseEvent = (xcb_key_release_event_t *)event;
            xcb_keysym_t key = keySymbols.keyCodeToKeySym(releaseEvent->detail, releaseEvent->state);
            GET_WINDOW_FROM_ID_OR_BREAK(releaseEvent->event, "key release", window)
            if (window->keyRelease((int)key, false)) {
                window->onUnusedKeyRelease((int)key, false);
            }
            break;
        }
        
        case XCB_CLIENT_MESSAGE: {
            xcb_client_message_event_t * messageEvent = (xcb_client_message_event_t *)event;
            GET_WINDOW_FROM_ID_OR_BREAK(messageEvent->window, "client message", window)
            
            const xcb_atom_t wmDeleteWindow = atomWmDeleteWindow.getAtom();
            
            // If we've got a valid wm delete window atom, then we know this is a window close
            // event if the event data matches the delete atom (WM_DELETE_WINDOW).
            if (wmDeleteWindow == XCB_ATOM_NONE) {
                std::cerr << "WARNING: rascUIxcb::Context: " << "client message" << " event sent to window without registered delete atom.\n";
            } else {
                if (messageEvent->data.data32[0] == wmDeleteWindow) {
                    window->onUserRequestClose();
                }
            }
        }
        
        case XCB_LEAVE_NOTIFY: {
            // When we get a leave notify, give the UI system a mouse move event to a position outside the window
            // (this is guaranteed by using the top level container location cache's outsideX and outsideY methods,
            // see rascUI::Rectangle).
            // NOTE: Don't complain about mouse leave events from unknown windows: These happen every time a window
            //       is closed/unmapped/destroyed - so of course we might not know about the window any more!
            xcb_leave_notify_event_t * leaveEvent = (xcb_leave_notify_event_t *)event;
            auto iter = windowsById.find(leaveEvent->event);
            if (iter != windowsById.end()) {
                Window * window = iter->second;
                GLfloat x = window->location.cache.outsideX(),
                        y = window->location.cache.outsideY();
                if (window->mouseMove(x, y)) {
                    window->onUnusedMouseMove(x, y);
                }
            }
            break;
        }
        
        case XCB_MAP_NOTIFY: {
            // For map notify, just notify the window by running the relevant method.
            xcb_map_notify_event_t * mapNotifyEvent = (xcb_map_notify_event_t *)event;
            GET_WINDOW_FROM_ID_OR_BREAK(mapNotifyEvent->window, "map notify", window)
            window->xcbOnMapNotify();
            break;
        }
        
        default: {
            // Deal with extension events - event IDs are not constants.
            // Ignore unknown events.
            
            // Randr extension.
            uint8_t randrBaseEventId;
            if (screenConfig.hasRandrExtension(randrBaseEventId)) {
                switch((int)eventResponseType - randrBaseEventId) {
                    
                    // What I've worked out from here:
                    // https://xcb.freedesktop.org/manual/group__XCB__RandR__API.html
                    // http://web.archive.org/web/20230607185223/https://xcb.freedesktop.org/manual/group__XCB__RandR__API.html
                    // To listen for CRTC change (xcb_randr_crtc_change_t), output change (xcb_randr_output_change_t),
                    // etc, you've got to listen for xcb_randr_notify_event_t, i.e: XCB_RANDR_NOTIFY.
                    // In the protocol, each event is XCB_RANDR_NOTIFY (1) from base event id, and is differentiated by "sub-code".
                    case XCB_RANDR_NOTIFY: {
                        // Re-request screen resources, to keep stuff
                        // up-to-date. Don't wait for a reply, as we don't know
                        // if the result is ever even going to be used.
                        screenConfig.requestScreenResources();
                        break;
                    }
                }
            }
            break;
        }

        }
    }

    void Context::xcbMainLoopRepaintAndSync(void) {
        // The afterEvent function queue must be updated BEFORE repainting: actions may request that stuff gets repainted.
        // Note that when events are skipped, afterEvent is not updated because it will be updated by the next event.
        afterEvent.update();
        
        // Repaint ALL (mapped) windows which want repainting, but only if there are no events waiting.
        // If there are, we should process *them* before doing any expensive drawing operations, then we will do the
        // drawing after processing the *next* event. This stops us falling behind if the user gives us more input
        // than we can draw: like scrolling really quickly - since drawing is the most expensive operation.
        if (connection.peekNextEvent() == NULL) {
            xcbOnMainLoopDraw();
            for (const std::pair<const xcb_window_t, Window *> & pair : windowsById) {
                if (pair.second->isMapped() && pair.second->getRepaintController()->isRepaintRequired()) {
                    pair.second->xcbDoPartialRepaintIfNotBlocked(0.1f);
                }
            }
            // Do a sync rather than a flush, to avoid our draw events ending in up a big pile.
            xcb_aux_sync(xcbGetConnection());
        }
    }

    void Context::xcbOnMainLoopDraw(void) {
    }

    void Context::xcbMainLoopCleanUp(void) {
        // Make sure this gets updated, even on the last iteration after exiting the loop.
        afterEvent.update();
        
        // Reset the file descriptors of the function queues, now the main loop is over.
        beforeEvent.setUpdateFileDescriptor();
        afterEvent.setUpdateFileDescriptor();
    }
}
