/*
 * Context.h
 *
 *  Created on: 15 May 2020
 *      Author: wilson
 */

#ifndef RASCUIXCB_PLATFORM_CONTEXT_H_
#define RASCUIXCB_PLATFORM_CONTEXT_H_

#include <rascUItheme/utils/DynamicThemeLoader.h>
#include <rascUI/platform/ContextBase.h>
#include <unordered_map>
#include <xcb/xcb.h>

#include "../util/ScreenConfig.h"
#include "../util/Connection.h"
#include "../util/KeySymbols.h"
#include "../util/Atom.h"

namespace rascUIxcb {
    class Window;

    /**
     * In rascUIxcb, context holds an instance of Connection, and manages
     * the screen, key symbols and main loop. Connection holds the connection
     * to the X server, and manages reading events from it.
     */
    class Context : public rascUI::ContextBase {
    public:
        /**
         * Filename of the config file used to select theme and UI scale,
         * when we're constructed "automatically". This resides within
         * the directory reported by PathUtils::getRascUIConfigDirectory.
         */
        static const char * const CONFIG_FILENAME;
        
        /**
         * Directory inside which we look for themes, when we're
         * constructed "automatically". Only top-most part of theme
         * filename from the config file is used, to ensure only themes
         * from within this directory are used.
         * NOTE: This is ALWAYS either empty (working directory), or a directory
         * with a trailing forward slash.
         * 
         * This is set, at compile time, to whatever the macro
         * RASCUIXCB_THEME_PATH is defined as, (and that macro is
         * assumed to be a STRING LITERAL, to avoid the contents of it
         * being expanded). If that macro is not defined, this is set to empty
         * string (and so the working directory is used as a theme
         * directory, which is only helpful for development).
         * When compiling for a modern linux distribution, to enable
         * multiarch support, it is recommended that RASCUIXCB_THEME_PATH
         * is defined by project configuration to the following:
         * /usr/lib/<triplet>/rascUI/themes/xcb
         * Where triplet is is the "multiarch host triplet" - something
         * like i386-linux-gnu or x86_64-linux-gnu
         */
        static const char * const THEME_DIRECTORY;
        
    private:
        // This allows windows to run the registerWindow/unregisterWindow methods.
        friend class Window;
        
        // Theme loader, used only when constructed "automatically".
        // **HACK:** This must exist before the Connection, to ensure dynamic libraries for themes
        // we load are unloaded AFTER the Xlib connection. Otherwise, libraries which themes load
        // (like Xft) are unloaded BEFORE the Xlib connection is closed. This is a problem since the
        // library in question (Xft) adds itself as an Xlib extension, and doesn't have any cleanup
        // code to undo that. Then, when we call XCloseDisplay, it gets a callback after the library
        // has been unloaded from memory.
        rascUItheme::DynamicThemeLoader autoConstructThemeLoader;
        
        // Our connection to the X server. The Connection class automates reading from it.
        Connection connection;
        
        // The current X screen.
        xcb_screen_t * screen;
        
        // Utilities: Key symbols table, screen config.
        KeySymbols keySymbols;
        ScreenConfig screenConfig;
        
        // The map from window IDs to registered windows.
        std::unordered_map<xcb_window_t, Window *> windowsById;
        
        // Ui scale, used only when constructed "automatically".
        GLfloat autoConstructUiScale;
        
        // Atoms we request by default and make available, see accessor methods
        // below. Note: We always create these if they don't already exist.
        Atom atomWmProtocols, atomWmDeleteWindow;
        
        // Whether double buffering should be enabled for windows using this
        // context. When constructed automatically, this option is loaded from
        // config, otherwise defaults to true and can be set with
        // xcbSetDoubleBufferEnabled.
        // Note that double buffering, (and whether to comply with this),
        // is entirely implemented by themes.
        bool doubleBufferEnabled;
        
        // Hint to themes about whether to draw a background in windows.
        // Typically this is used to show where components are missing as a
        // debug feature, but is unnecessary the rest of the time.
        // Loaded from config when auto-constructed, else false by default.
        // Can be set with "xcbSetDrawWindowBackgrounds".
        bool drawWindowBackgrounds;
        
        // Set to true if auto constructing failed.
        bool autoConstructFailed;
        
    protected:
        /**
         * Whether to continue executing the main loop. This is set to
         * false by endMainLoop. Protected for use by custom main loops.
         */
        bool continueMainLoop;
        
    public:
        /**
         * The main constructor for rascUIxcb::Context. Note that by default
         * we use rascUI::Bindings::defaultX11, and require partial repainting
         * to be enabled.
         * 
         * See xcb_connect documentation for details of displayname.
         * If left as the default values, the default X display will be used.
         */
        Context(rascUI::Theme * defaultTheme,
                const GLfloat * defaultXScalePtr = NULL,
                const GLfloat * defaultYScalePtr = NULL,
                const rascUI::Bindings * defaultBindings = NULL,
                const char * displayname = NULL);
        
        /**
         * "Automatically" construct a context, loading theme and UI
         * scale according to the rascUIxcb config file.
         * 
         * If "runInitialise" is passed as false xcbInitialise won't be called
         * in this constructor, running that is left as the caller's
         * responsibility. See documentation for xcbInitialise.
         * 
         * See xcb_connect documentation for details of displayname.
         * If left as the default values, the default X display will be used.
         */
        Context(bool runInitialise = true, const char * displayname = NULL);
        
        virtual ~Context(void);
        
    private:
        // Run by Window at the end of it's constructor.
        void registerWindow(Window * window);
        
        // Run by Window at the start of it's destructor.
        void unregisterWindow(Window * window);
    
    public:
        
        /**
         * Here we return true if the X connection worked, and it has no errors.
         */
        virtual bool wasSuccessful(void) const override;
        
        /**
         * Here we flush the connection to the X server.
         */
        virtual void flush(void) override;
        
        /**
         * Our implementation of the main event loop. This waits for
         * events from the X server, and processes them as necessary.
         * This can be customised (by subclasses) using the xcbMainLoop*
         * methods below. Custom main loop implementations should
         * match the implementation of Context::mainLoop, although can
         * add custom event handling or other loop logic.
         */
        virtual unsigned long long mainLoop(void) override;

        /**
         * Ends the main event loop.
         */
        virtual void endMainLoop(void) override;
        
        /**
         * Repaints all (mapped) windows.
         */
        virtual void repaintEverything(void) override;
        
        // ---------------------- XCB specific methods -------------------------
        
        /**
         * Allows access to our X connection.
         */
        inline xcb_connection_t * xcbGetConnection(void) const {
            return connection.getConnection();
        }
        
        /**
         * Allows access to our X screen.
         */
        inline xcb_screen_t * xcbGetScreen(void) const {
            return screen;
        }
        
        /**
         * Allows access to our rascUIxcb connection.
         */
        inline const Connection & xcbGetXcbConnection(void) const {
            return connection;
        }
        inline Connection & xcbGetXcbConnection(void) {
            return connection;
        }
        
        /**
         * Allows access to our table of key symbols.
         */
        inline const KeySymbols & xcbGetKeySymbols(void) const {
            return keySymbols;
        }
        
        /**
         * Allows access to our screen config, which provides convenient access
         * to screen geometry.
         */
        inline const ScreenConfig & xcbGetScreenConfig(void) const {
            return screenConfig;
        }
        inline ScreenConfig & xcbGetScreenConfig(void) {
            return screenConfig;
        }
        
        /**
         * Allows access to our Xlib display. This is provided for compatibility
         * with Xlib functions: the xcb connection from xcbGetConnection should
         * always be preferred.
         */
        inline Display * xcbGetXlibDisplay(void) const {
            return connection.getXlibDisplay();
        }
        
        /**
         * Returns whether double buffering should be enabled for windows using
         * this context, see doubleBufferEnabled.
         */
        inline bool xcbIsDoubleBufferEnabled(void) const {
            return doubleBufferEnabled;
        }
        
        /**
         * Sets whether double buffering should be enabled for windows using
         * this context, see doubleBufferEnabled.
         */
        inline void xcbSetDoubleBufferEnabled(const bool doubleBufferEnabled) {
            this->doubleBufferEnabled = doubleBufferEnabled;
        }
        
        /**
         * Returns whether window backgrounds should be drawn - this is a hint
         * to themes. Typically this is used to show where components are
         * missing as a debug feature, but is unnecessary the rest of the time.
         */
        inline bool xcbShouldDrawWindowBackgrounds(void) const {
            return drawWindowBackgrounds;
        }
        
        /**
         * Set whether window backgrounds should be drawn.
         */
        inline void xcbSetDrawWindowBackgrounds(const bool drawWindowBackgrounds) {
            this->drawWindowBackgrounds = drawWindowBackgrounds;
        }
        
        /**
         * Returns the number of windows which are currently mapped.
         */
        size_t xcbCountMappedWindows(void) const;
        
        /**
         * Accessor methods to get atoms we've requested by default.
         */
        inline Atom & xcbGetAtomWmProtocols(void) {
            return atomWmProtocols;
        }
        inline Atom & xcbGetAtomWmDeleteWindow(void) {
            return atomWmDeleteWindow;
        }
        
        /**
         * Returns a pointer to a registered window by xcb id, if it exists.
         * Else returns null.
         */
        Window * xcbGetWindowById(const xcb_window_t windowId) const;
        
        /**
         * This can be called by the theme, to set the intended cursor for the
         * display's root window.
         * If the theme wants to do this, it should be called in the theme's
         * init method, (and again with XCB_CURSOR_NONE in the theme's destroy
         * method to clean up).
         * By default, this does nothing (rascUIxcb does NOT implement this!).
         * But, if the theme is attached to a window manager using rascUI and
         * rascUIxcb, the window manager can implement this to provide a default
         * root cursor chosen by the theme.
         */
        virtual void setRootCursor(xcb_cursor_t rootCursor);
        
    protected:
        /**
         * Common initialisation stuff run by both constructors, notably this
         * does the theme's init method (if theme is constructed automatically).
         * Called by default unless the relevant bool flag is set to false (see
         * documentation in constructors) - then it is the responsibility of the
         * caller (subclass) to call this. Should be called even if not
         * successful.
         * A custom Context subclass may wish to manually call this if they want
         * to do some setup (or override virtual methods), used by the theme's
         * init method, (e.g: if they implement setRootCursor).
         */
        void xcbInitialise(void);
    
        // ------------------- (XCB) custom main loop methods -------------------
        
        /**
         * Must be run at the start of the main loop, initialises things
         * like pipes and function queues.
         */
        void xcbMainLoopInit(void);
        
        /**
         * Gets event using function queue logic, possibly setting error
         * status.
         */
        void xcbMainLoopGetEvent(xcb_generic_event_t ** event, bool * error);
        
        /**
         * Handles the specified event (assumed not to be null). In
         * custom main loops, this only needs calling if the event has
         * not already been handled.
         */
        void xcbMainLoopHandleEvent(xcb_generic_event_t * event, bool * repaintNeeded);
        
        /**
         * If there are no incoming events pending, calls xcbOnMainLoopDraw,
         * repaints each window in need of repainting, updates after event
         * function queue, and does an xcb_aux_sync if there are no pending
         * events. This does not need calling in the cases where repainting is
         * skipped (duplicate events).
         */
        void xcbMainLoopRepaintAndSync(void);
        
        /**
         * Called by xcbMainLoopRepaintAndSync if it *actually* decides to do
         * drawing, (i.e: if there are no more events pending).
         * Custom Context implementations may want to override this, to perform
         * any (slow) operations where we don't want them to "pile up", when
         * lots of events are being processed.
         */
        virtual void xcbOnMainLoopDraw(void);
        
        /**
         * Must be run at the end of the main loop, cleans up resources
         * created in xcbMainLoopInit.
         */
        void xcbMainLoopCleanUp(void);
    };
}

#endif
