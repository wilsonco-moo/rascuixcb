/*
 * Window.cpp
 *
 *  Created on: 15 May 2020
 *      Author: wilson
 */

#include "Window.h"

#include <rascUI/platform/WindowConfig.h>
#include <xcb/xproto.h>
#include <iostream>

#include "../util/ScreenConfig.h"
#include "../util/Misc.h"
#include "Context.h"

namespace rascUIxcb {

    Window::Window(Context * context, const rascUI::WindowConfig & config, rascUI::Theme * theme, const rascUI::Location & location, const rascUI::Bindings * bindings, const GLfloat * xScalePtr, const GLfloat * yScalePtr) :
        rascUI::WindowBase((rascUI::ContextBase *)context, config, theme, location, bindings, xScalePtr, yScalePtr),
        offScreenBuffer((xcb_pixmap_t)-1),
        lastCursor(XCB_CURSOR_NONE),
        // X/Y always zero, use config width/height. Can initialise here as config
        // width and height is always honoured.
        windowDrawArea(0.0f, 0.0f, config.screenWidth, config.screenHeight),
        receivedExposeOrMapNotifyEvent(false),
        hasPendingUnmap(false) {
        
        // Set the TopLevelContainer before draw theme user data to a pointer to ourself:
        // this allows the Theme to be able to draw to us.
        beforeDrawThemeUserData = this;
        
        // For convenience.
        xcb_connection_t * connection = context->xcbGetConnection();
        xcb_screen_t * screen = context->xcbGetScreen();
        
        // Generate IDs for our graphics context and window.
        graphics = xcb_generate_id(connection);
        window = xcb_generate_id(connection);
    
        // -------------------- Initialise graphics context --------------------
        {
            // Set foreground colour to black, set XCB_GC_GRAPHICS_EXPOSURES to zero.
            uint32_t mask     =   XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES,
                     values[] = {screen->black_pixel, 0};

            xcb_create_gc(
                connection,   // Connection.
                graphics,     // Graphics context id.
                screen->root, // A drawable - the screen.
                mask,         // The mask.
                values);      // The value.
        }
        
        // ------------------------- Window placement --------------------------
        
        // Always use specified width/height (clamped). Use specified x/y by
        // default.
        xcb_rectangle_t windowDimensions;
        windowDimensions.width = Misc::clampToPureSize16(config.screenWidth);
        windowDimensions.height = Misc::clampToPureSize16(config.screenHeight);
        windowDimensions.x = Misc::clampToPureInt16(config.screenX);
        windowDimensions.y = Misc::clampToPureInt16(config.screenY);
        
        // If user wants us to decide a position for them, find the active CRTC
        // under the mouse pointer, and place within that. Unfortunately, when
        // multiple CRTCs are available, this requires a round-trip!
        if (!config.enableScreenPosition) {
            const ActiveCrtc * const activeCrtc = context->xcbGetScreenConfig().getActiveCrtcUnderPointer();
            // Might be null (if xrandr isn't loaded, or headless X?).
            if (activeCrtc != NULL) {
                activeCrtc->place(windowDimensions);
            }
        }
        
        // Initialise geometry area now we know initial placement.
        windowGeometryArea = Misc::xcbToRascUIRect(windowDimensions);
        
        // -------------------------- Initialise window ------------------------
        {
            // Enable the various events specified by the base event mask.
            const uint32_t mask = xcbGetBaseEventMask();

            xcb_create_window(
                connection,                                       // The connection.
                XCB_COPY_FROM_PARENT,                             // The depth.
                window,                                           // The window id.
                screen->root,                                     // The parent window (the root window).
                windowDimensions.x,                               // x.
                windowDimensions.y,                               // y.
                windowDimensions.width,                           // width.
                windowDimensions.height,                          // height.
                config.borderWidth,                               // border width.
                XCB_WINDOW_CLASS_INPUT_OUTPUT,                    // The window class (type).
                screen->root_visual,                              // Visual for the window.
                XCB_CW_EVENT_MASK, &mask);                        // The mask and values to enable events.
        }
        
        // Set the window title.
        xcb_change_property(connection,             // The connection.
                            XCB_PROP_MODE_REPLACE,  // Replacement mode: discard the old value.
                            window,                 // The window ID.
                            XCB_ATOM_WM_NAME,       // The property to change (as an atom).
                            XCB_ATOM_STRING,        // The data type of the property (as an atom).
                            8,                      // The format of the data (8 bit bytes), so the X server can do byte swapping.
                            config.title.length(),  // The length (in number of elements from format) of the data.
                            config.title.c_str());  // The data to use for setting the title (the title string).
        
        // According to documentation (see below),
        // to receive close events, we need to get the atom called "WM_DELETE_WINDOW"
        // and include it in the "WM_PROTOCOLS" property of our window. If any more "WM_PROTOCOLS"
        // are required in the future, they should also be added here.
        //
        // Ok to do it on window creation: No round-trip delay for each window
        // as context has already queried the atoms, and all windows will use
        // the same ones.
        //
        // The following code has been adapted from: https://github.com/Niko40/VulkanBuildupPractice/blob/master/Window_xcb.cpp
        //                                           http://web.archive.org/web/20200517151010/https://github.com/Niko40/VulkanBuildupPractice/blob/master/Window_xcb.cpp
        //
        // When an XCB_CLIENT_MESSAGE event is encountered, which has
        // WM_DELETE_WINDOW a data[0] field, it should be interpreted as
        // a window delete message.
        //
        //      See: https://tronche.com/gui/x/icccm/sec-4.html#s-4.2.8.1
        //           http://web.archive.org/web/20191102124643/https://tronche.com/gui/x/icccm/sec-4.html#s-4.2.8.1
        // Also see: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.7
        //           http://web.archive.org/web/20191102124643/https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.7
        // And this: https://stackoverflow.com/a/22612855
        //           http://web.archive.org/web/20200329061353/https://stackoverflow.com/questions/10792361/how-do-i-gracefully-exit-an-x11-event-loop
        //
        // Grab the atoms from context and complain if they don't exist.
        const xcb_atom_t wmProtocols = context->xcbGetAtomWmProtocols().getAtom();
        const xcb_atom_t wmDeleteWindow = context->xcbGetAtomWmDeleteWindow().getAtom();
        if (wmProtocols == XCB_ATOM_NONE || wmDeleteWindow == XCB_ATOM_NONE) {
            std::cerr << "WARNING: rascUIxcb::Window: Failed to access atoms to set window close behaviour.\n";
        } else {
            // If atoms are valid, set them in the window.
            xcb_change_property(context->xcbGetConnection(),       // The connection.
                                XCB_PROP_MODE_APPEND,              // Replacement mode: append, since we might be subscribed to other WM_PROTOCOLS already.
                                window,                            // The window ID.
                                wmProtocols,                       // The property to change (as an atom).
                                XCB_ATOM_ATOM,                     // The data type of the property (as an atom): we are sending an atom.
                                32,                                // The format of the data (this is an atom ID, so 32 bit integer), so the X server can do byte swapping.
                                1,                                 // The length (in number of elements from format) of the data.
                                &wmDeleteWindow);                  // The data to use: The atom we want to set it to.
        }
        
        // Now that we are completely configured, register with our context.
        context->registerWindow(this);
    }
    
    Window::~Window(void) {
        // Unregister from our context, before we do any freeing.
        ((Context *)getContext())->unregisterWindow(this);
        
        // For convenience.
        xcb_connection_t * connection = ((Context *)getContext())->xcbGetConnection();
        // Free the off screen buffer pixmap (if necessary).
        if (offScreenBuffer != ((xcb_pixmap_t)-1)) {
            xcb_free_pixmap(connection, offScreenBuffer);
        }
        // Free the graphics context and the window. This last step will
        // automatically unmap the window if it was still mapped.
        xcb_free_gc(connection, graphics);
        xcb_destroy_window(connection, window);
    }
    
    void Window::onExposeOrMapNotify(void) {
        if (hasPendingUnmap) {
            // Unmap is pending, because we unmapped before receiving expose
            // or map notify event: Unmap the window now, clear the pending
            // unmap flag.
            xcbRequestUnmapWindow();
            hasPendingUnmap = false;
        
        } else if (isMapped()) {
            // Otherwise, set flag to say we've received our first expose or
            // map notify event. Only do this if we're *intended* to be mapped:
            // It is possible to receive multiple events after requesting an
            // unmap, and we don't want this flag true while unmapped!
            receivedExposeOrMapNotifyEvent = true;
        }
    }
    
    void Window::onMapWindow(void) {
        if (hasPendingUnmap) {
            // We've got an unmap pending - this means we're still mapped. Clear
            // the flag and do nothing.
            hasPendingUnmap = false;
        
        } else {
            // Otherwise map the window normally.
            xcbRequestMapWindow();
        }
    }
    
    void Window::onUnmapWindow(void) {
        if (receivedExposeOrMapNotifyEvent) {
            // Already received expose or map notify: Unmap normally, clear
            // flag to reset for next time.
            xcbRequestUnmapWindow();
            receivedExposeOrMapNotifyEvent = false;
            
        } else {
            // Not received expose or map notify yet: Defer the unmap.
            hasPendingUnmap = true;
        }
    }
    
    void Window::xcbRequestMapWindow(void) {
        xcb_map_window(((Context *)getContext())->xcbGetConnection(), window);
    }
    
    void Window::xcbRequestUnmapWindow(void) {
        xcb_unmap_window(((Context *)getContext())->xcbGetConnection(), window);
    }
    
    void Window::xcbOnReceiveExposeEvent(void) {
        onExposeOrMapNotify();
    }

    void Window::xcbDoPartialRepaintIfNotBlocked(GLfloat elapsed) {
        if (receivedExposeOrMapNotifyEvent) {
            doPartialRepaint(windowDrawArea, elapsed);
        }
    }
    
    uint32_t Window::xcbGetBaseEventMask(void) {
        return XCB_EVENT_MASK_EXPOSURE |
            XCB_EVENT_MASK_STRUCTURE_NOTIFY | // (Configure notify).
            XCB_EVENT_MASK_POINTER_MOTION |
            XCB_EVENT_MASK_BUTTON_MOTION |
            XCB_EVENT_MASK_ENTER_WINDOW |
            XCB_EVENT_MASK_LEAVE_WINDOW |
            XCB_EVENT_MASK_BUTTON_PRESS |
            XCB_EVENT_MASK_BUTTON_RELEASE |
            XCB_EVENT_MASK_KEY_PRESS |
            XCB_EVENT_MASK_KEY_RELEASE;
    }
    
    void Window::xcbOnMapNotify(void) {
        onExposeOrMapNotify();
    }
}
