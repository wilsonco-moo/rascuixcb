/*
 * Window.h
 *
 *  Created on: 15 May 2020
 *      Author: wilson
 */

#ifndef RASCUIXCB_PLATFORM_WINDOW_H_
#define RASCUIXCB_PLATFORM_WINDOW_H_

#include <rascUI/platform/WindowBase.h>
#include <xcb/xcb.h>
#include <cstddef>

namespace rascUIxcb {
    class Context;

    /**
     * XCB implementation of the rascUI platform window.
     */
    class Window : public rascUI::WindowBase {
    private:
        // The id for our window's graphics context. This is allocated and
        // managed by the window.
        xcb_gcontext_t graphics;
        // The id for our window. This is allocated and managed by the window.
        xcb_window_t window;
        // This should be used by themes for an off screen buffer to draw to.
        // If this exists (i.e: is not (xcb_pixmap_t)-1) when the window is
        // destroyed, the window will automatically free this.
        // This is set to (xcb_pixmap_t)-1 by default, and is intended to be
        // modified by themes.
        xcb_pixmap_t offScreenBuffer;
        // This should be used by themes to keep track of the last cursor used
        // by this window, (XCB_CURSOR_NONE by default).
        // Usage is entirely optional: Provided only for convenience, to avoid
        // needing to set cursor constantly.
        xcb_cursor_t lastCursor;
        // The area WITHIN the window, which should be passed to our
        // TopLevelContainer draw methods. This is set from configure notify
        // events, and the window's initial placement.
        rascUI::Rectangle windowDrawArea;
        // The area of this X window according to the latest configure notify
        // event we received. Also set to initial placement when window is
        // created. Typically, size is identical to windowDrawArea, and x/y is
        // EITHER relative to root or parent window, (window manager may say
        // either). Typically, only needed for knowing configured sizes *within*
        // a rascUI based window manager.
        rascUI::Rectangle windowGeometryArea;
        // Set to true once onExposeOrMapNotify runs. This allows us to block
        // repaint requests, until AFTER we receive our first expose or
        // map notify event (see xcbDoPartialRepaintIfNotBlocked).
        // It also allows us to delay unmaps until after the first expose or map
        // notify event, (see comments on onUnmapWindow).
        bool receivedExposeOrMapNotifyEvent;
        // Whether an unmap request is pending (was skipped due to being
        // requested before expose). This avoids the MapRequest/UnmapNotify race
        // condition (see comments on onUnmapWindow).
        bool hasPendingUnmap;
        
    public:
        Window(Context * context, const rascUI::WindowConfig & config,
               rascUI::Theme * theme = NULL, const rascUI::Location & location = rascUI::Location(),
               const rascUI::Bindings * bindings = NULL,
               const GLfloat * xScalePtr = NULL, const GLfloat * yScalePtr = NULL);
        
        virtual ~Window(void);
        
    private:
        // Called when we receive an expose or map notify event, required for
        // dealing with the MapRequest/UnmapNotify race condition (see comments
        // on onUnmapWindow).
        void onExposeOrMapNotify(void);
        
    protected:
        /**
         * Normally, here we simply map the xcb window.
         * If an unmap request is pending (see onUnmapWindow), that gets
         * cancelled and we do nothing.
         */
        virtual void onMapWindow(void) override;
        /**
         * Normally, here we simply unmap the xcb window.
         * Unfortunately, this is complicated by a race condition inherent to
         * how Xorg requires window managers to work.
         * xcb_map_window doesn't map the window immediately, it is passed on to
         * the window manager as an xcb_map_request_event_t event.
         * Meanwhile xcb_unmap_window DOES unmap the window immediately, passing
         * an xcb_unmap_notify_event_t to the window manager.
         * This means, if the client application maps then immediately unmaps
         * a window, the unmap can be processed on the X server BEFORE the
         * window manager has processed xcb_map_request_event_t (and mapped the
         * window). As far as X server is concerned, you're unmapping a window
         * which hasn't been mapped yet - so it does nothing, AND never sends
         * xcb_unmap_notify_event_t!
         * That can leave you stuck with a "zombie" mapped window, after you've
         * told it to unmap. Obviously it isn't acceptable to expect rascUIxcb
         * users to deal with this, so we must handle this internally. The only
         * reference I can find to someone else running into this is here, they
         * recommend waiting for the first expose event before unmapping,
         * (though we use map notify too, as expose may not arrive if the window
         * is placed behind another!).
         * https://stackoverflow.com/questions/19413806/xcb-window-will-not-unmap-after-being-mapped-once
         * http://web.archive.org/web/20151017234915/https://stackoverflow.com/questions/19413806/xcb-window-will-not-unmap-after-being-mapped-once
         * So that's what we do here: If we've not received an expose (or map
         * notify) event yet, we set hasPendingUnmap instead of unmapping. Once
         * we receive expose (or map notify, see onExposeOrMapNotify), we'll
         * unmap if pending. If we're told to map again first, it is cancelled.
         * This complicates cases where a user might want to customise how the
         * map or unmap requests are sent (there are specific cases in window
         * managers where this is needed). In those cases, the user should
         * override xcbRequestMapWindow/xcbRequestUnmapWindow instead, to avoid
         * affecting the "pending unmap" logic. See below.
         */
        virtual void onUnmapWindow(void) override;

        // ---------------------- XCB specific methods -------------------------

        /**
         * By default, all these do are xcb_map_window/xcb_unmap_window. These
         * are used by onMapWindow/onUnmapWindow to request map/unmap - but due
         * to the pending unmap stuff, they may not always be called (see
         * comments above).
         * If a user wants to customise how the map/unmap requests are done,
         * these should be overidden instead of onMapWindow/onUnmapWindow.
         * For example, a window manager may want to send itself a map request
         * event instead of directly mapping the window, if it wants to manage
         * a window which it has created.
         */
        virtual void xcbRequestMapWindow(void);
        virtual void xcbRequestUnmapWindow(void);

    public:
        /**
         * Allows access to our related graphics context.
         */
        inline xcb_gcontext_t xcbGetGraphics(void) const {
            return graphics;
        }
        
        /**
         * Allows access to our xcb window ID.
         */
        inline xcb_window_t xcbGetWindow(void) const {
            return window;
        }
        
        /**
         * Allows access to, and modification of, our off screen buffer
         * pixmap ID. (See comments for offScreenBuffer field).
         */
        inline xcb_pixmap_t & xcbGetOffScreenBuffer(void) {
            return offScreenBuffer;
        }
        
        /**
         * Allows access to, and modification of, our last cursor id. (See
         * comments for lastCursor field).
         */
        inline xcb_cursor_t & xcbGetLastCursor(void) {
            return lastCursor;
        }
        
        /**
         * Allows access to, and modification of, our window draw area.
         * See documentation further up.
         */
        inline rascUI::Rectangle & xcbGetWindowDrawArea(void) {
            return windowDrawArea;
        }
        
        /**
         * Allows access to, and modification of, our window geometry area.
         * See documentation further up.
         */
        inline rascUI::Rectangle & xcbGetWindowGeometryArea(void) {
            return windowGeometryArea;
        }
        
        /**
         * This should be run by Context each time we receive an expose event,
         * after setting the draw area.
         * Requests to repaint are blocked between mapping the window, and
         * receiving the first expose (or map notify) event. See documentation
         * for xcbDoPartialRepaintIfNotBlocked.
         * Runs onExposeOrMapNotify.
         */
        void xcbOnReceiveExposeEvent(void);
        
        /**
         * This performs a partial repaint, using our internal window draw area,
         * but only if we have ALREADY received an expose (or map notify) event.
         * This has the effect of blocking repaint requests, until we get our
         * first expose (or map notify) event.
         * 
         * Repaints must be blocked until we get the first expose (or map
         * notify) event for the following reasons:
         *  > This makes no difference if the window is mapped at the very
         *    start. If a window is mapped during an event (like when a button
         *    or key is pressed), it will be repainted at the end of that
         *    event, THEN repainted again when it gets its expose event.
         *  > The geometry of the window (windowDrawArea) is queried from the
         *    server, during expose events. As a result, any painting done
         *    before that would cause the theme to get an onChangeViewArea
         *    call, with the default constructed (zero) window draw area.
         *    This results in the creation of a zero size pixel buffer, for the
         *    window's offScreenBuffer -> causing errors and problems.
         *  > Painting to a window before its expose event, or performing
         *    actions like creating a pixel buffer from the window, also
         *    seems to cause errors and problems.
         */
        void xcbDoPartialRepaintIfNotBlocked(GLfloat elapsed);
        
        /**
         * Gets the base event mask required by a rascUIxcb window to have all
         * functionality, (i.e: XCB_CW_EVENT_MASK). Any custom window
         * implementations which change the event mask, should also include
         * these flags if they want components etc to work properly.
         */
        static uint32_t xcbGetBaseEventMask(void);
        
        /**
         * Called when we receive map notify events for our window, i.e: Window
         * manager has responded to our map request. This is a more reliable
         * way (than expose event) to check that the window manager has created
         * the window, as we may never actually get an expose event if the
         * window has been placed behind another!
         * Custom window implementations can override this - but make sure
         * the parent method is still called!
         */
        virtual void xcbOnMapNotify(void);
    };
}

#endif
