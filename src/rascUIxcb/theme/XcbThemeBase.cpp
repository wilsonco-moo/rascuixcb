/*
 * XcbThemeBase.cpp
 *
 *  Created on: 7 Jun 2019
 *      Author: wilson
 */

#include "XcbThemeBase.h"

#include <rascUI/base/TopLevelContainer.h>
#include <cstddef>

#include "../platform/Context.h"
#include "../platform/Window.h"

namespace rascUIxcb {
    
    XcbThemeBase::BufferData::BufferData(void) :
        drawable((xcb_drawable_t)-1),
        graphics((xcb_gcontext_t)-1),
        connection(NULL) {
    }
    
    XcbThemeBase::XcbThemeBase(void) :
        rascUI::Theme(),
        context(NULL),
        window(NULL),
        bufferData() {
    }

    XcbThemeBase::~XcbThemeBase(void) {
    }

    void XcbThemeBase::init(void * userData) {
        context = (Context *)userData;
        bufferData.connection = context->xcbGetConnection();
    }
    
    void XcbThemeBase::beforeDraw(void * userData) {
        window = (Window *)userData;
        // Use either the off screen buffer or the window itself as drawing,
        // depending on double buffer status.
        if (context->xcbIsDoubleBufferEnabled()) {
            bufferData.drawable = window->xcbGetOffScreenBuffer();
        } else {
            bufferData.drawable = window->xcbGetWindow();
        }
        bufferData.graphics = window->xcbGetGraphics();
    }
    
    void XcbThemeBase::afterDraw(const rascUI::Rectangle & drawnArea) {
        
        // If double buffering is enabled, we need to copy the off-screen buffer
        // to the window.
        if (context->xcbIsDoubleBufferEnabled()) {
            
            // Clip the draw area with the TopLevelContainer last view, so we don't go out of bounds.
            rascUI::Rectangle copyArea = drawnArea * window->getLastView();

            // If this clipped area actually exists, copy from the off-screen buffer to the window.
            if (copyArea.isNonZero()) {
                xcb_copy_area(
                    bufferData.connection,      // The connection to use
                    bufferData.drawable,        // The source drawable
                    window->xcbGetWindow(),     // The destination drawable
                    bufferData.graphics,        // The graphics context
                    (int16_t)copyArea.x,        // Source X
                    (int16_t)copyArea.y,        // Source Y
                    (int16_t)copyArea.x,        // Destination X
                    (int16_t)copyArea.y,        // Destination Y
                    (uint16_t)copyArea.width,   // Width of area
                    (uint16_t)copyArea.height   // Height of area
                );
            }
        }
        
        // After finishing drawing, relinquish anything relating to the window.
        window = NULL;
        bufferData.drawable = (xcb_drawable_t)-1;
        bufferData.graphics = (xcb_gcontext_t)-1;
    }
    
    void XcbThemeBase::destroy(void) {
        // After destroy, relinquish anything relating to the context.
        context = NULL;
        bufferData.connection = NULL;
    }
    
    void * XcbThemeBase::getDrawBuffer(void) {
        return &bufferData;
    }
    
    void XcbThemeBase::onChangeViewArea(const rascUI::Rectangle & view) {
        // If the context has double buffering enabled, we need to resize
        // the off screen buffer. Note that if double buffering is enabled,
        // "drawable" *is* the off-screen buffer.
        // This will also *allocate* the off-screen buffer the first time.
        if (context->xcbIsDoubleBufferEnabled()) {
            
            // If there was already an off-screen buffer (i.e: this isn't the
            // first time we have run this), then free it.
            if (bufferData.drawable != ((xcb_drawable_t)-1)) {
                xcb_free_pixmap(bufferData.connection, bufferData.drawable);
            }
            
            // Generate an ID.
            bufferData.drawable = xcb_generate_id(bufferData.connection);
            
            // Allocate the buffer. For it's size, use the size of the view
            // plus it's position (this way coordinates are consistent, plus
            // non-zero x and y values are very rare anyway).
            xcb_create_pixmap(
                bufferData.connection,               // The connection to use
                context->xcbGetScreen()->root_depth, // The colour depth (of the screen)
                bufferData.drawable,                 // The id of the pixmap to create
                window->xcbGetWindow(),              // The drawable to get screen information from (the window)
                (uint16_t)(view.x + view.width),     // Width of the pixmap
                (uint16_t)(view.y + view.height)     // Height of the pixmap
            );
            
            // Set the window's off screen buffer to this (so we can access it next time).
            window->xcbGetOffScreenBuffer() = bufferData.drawable;
        }
    }

    // Methods we override and do nothing, as to allow use in a ThemeGroup.
    void XcbThemeBase::redrawFromBuffer(void) {}
    void XcbThemeBase::drawBackPanel(const rascUI::Location & location) {}
    void XcbThemeBase::drawFrontPanel(const rascUI::Location & location) {}
    void XcbThemeBase::drawText(const rascUI::Location & location, const std::string & string) {}
    void XcbThemeBase::drawButton(const rascUI::Location & location) {}
    void XcbThemeBase::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {}
    void XcbThemeBase::drawScrollBarBackground(const rascUI::Location & location) {}
    void XcbThemeBase::drawScrollContentsBackground(const rascUI::Location & location) {}
    void XcbThemeBase::drawScrollPuck(const rascUI::Location & location, bool vertical) {}
    void XcbThemeBase::drawScrollUpButton(const rascUI::Location & location, bool vertical) {}
    void XcbThemeBase::drawScrollDownButton(const rascUI::Location & location, bool vertical) {}
    void XcbThemeBase::drawFadePanel(const rascUI::Location & location) {}
    void XcbThemeBase::drawCheckboxButton(const rascUI::Location & location) {}
    void XcbThemeBase::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {}
    void XcbThemeBase::drawTooltipBackground(const rascUI::Location & location) {}
    void XcbThemeBase::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {}
    void XcbThemeBase::drawWindowDecorationBackPanel(const rascUI::Location & location) {}
    void XcbThemeBase::drawWindowDecorationFrontPanel(const rascUI::Location & location) {}
    void XcbThemeBase::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {}
    void XcbThemeBase::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {}
    
    void XcbThemeBase::customSetDrawColour(void * colour) {}
    void * XcbThemeBase::customGetTextColour(const rascUI::Location & location) { return NULL; }
    void XcbThemeBase::customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string)  {}
    void XcbThemeBase::customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {}
    void XcbThemeBase::customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {}
    void XcbThemeBase::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {}
}
