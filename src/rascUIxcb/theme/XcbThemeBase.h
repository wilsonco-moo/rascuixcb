/*
 * XcbThemeBase.h
 *
 *  Created on: 7 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_THEME_XCBTHEMEBASE_H_
#define RASCUIXCB_THEME_XCBTHEMEBASE_H_

#include <rascUI/util/Rectangle.h>
#include <rascUI/base/Theme.h>
#include <xcb/xcb.h>

namespace rascUIxcb {
    class Context;
    class Window;

    /**
     * This is a base class for Themes in rascUIxcb. It automates basic operations
     * like double buffering.
     *
     * This does not (really?) work as a theme on its own. This is intended to either be used
     * as a base class for a theme, or be combined with another theme using a ThemeGroup.
     */
    class XcbThemeBase : public rascUI::Theme {
    public:
    
        /**
         * This class is accessed through our getDrawBuffer method. This
         * contains all context required for custom drawing.
         */
        class BufferData {
        private:
            friend class XcbThemeBase;
            
            // Set to the appropriate drawable for subclasses to draw to, and
            // for custom drawing. This is the window's off-screen buffer (when
            // double buffering is enabled), or the window otherwise.
            // When no window is currently in use (outside beforeDraw - afterDraw)
            // this is set to (xcb_gcontext_t)-1.
            xcb_drawable_t drawable;
            
            // The graphics context which is associated with the current window.
            // When no window is currently in use (outside beforeDraw - afterDraw)
            // this is set to (xcb_gcontext_t)-1.
            // This is provided for convenience, and for custom drawing.
            xcb_gcontext_t graphics;
            
            // The current connection.
            // When no context is currently in use (outside init - destroy) this is
            // set to NULL.
            // This is provided for convenience, and for custom drawing.
            xcb_connection_t * connection;
        
        public:
            BufferData(void);
            
            /**
             * The current connection.
             * When no context is currently in use (outside init - destroy) this is
             * set to NULL.
             * This is provided for convenience.
             */
            inline xcb_connection_t * getConnection(void) const {
                return connection;
            }
            
            /**
             * Gets the appropriate drawable for subclasses to draw to,
             * When no window is currently in use (outside beforeDraw - afterDraw)
             * this returns (xcb_gcontext_t)-1.
             */
            inline xcb_drawable_t getDrawable(void) const {
                return drawable;
            }
            
            /**
             * Gets the graphics context which is associated with the current window.
             * When no window is currently in use (outside beforeDraw - afterDraw)
             * this returns (xcb_gcontext_t)-1.
             * This is provided for convenience.
             */
            inline xcb_gcontext_t getGraphics(void) const {
                return graphics;
            }
        };
        
    private:
        // The current context that we are using.
        // When no context is currently in use (outside init - destroy) this is
        // set to NULL.
        Context * context;
        
        // The current window that we are drawing to.
        // When no window is currently in use (outside beforeDraw - afterDraw)
        // this is set to NULL.
        Window * window;
        
        // Buffer data.
        BufferData bufferData;
        
    public:
        /**
         * Note that it is the responsibility of subclasses to set properties
         * like normal component sizes, ui border, custom colours etc.
         */
        XcbThemeBase(void);
        virtual ~XcbThemeBase(void);

    protected:
        // -------------------- Accessor methods for subclasses ----------------
        
        /**
         * Gets the current context that we are using.
         * When no context is currently in use (outside init - destroy) this
         * returns NULL.
         */
        inline Context * getContext(void) const {
            return context;
        }
        
        /**
         * The current connection.
         * When no context is currently in use (outside init - destroy) this is
         * set to NULL.
         * This is provided for convenience.
         */
        inline xcb_connection_t * getConnection(void) const {
            return bufferData.connection;
        }
        
        /**
         * Gets the current window that we are drawing to.
         * When no window is currently in use (outside beforeDraw - afterDraw)
         * this returns NULL.
         */
        inline Window * getWindow(void) const {
            return window;
        }
        
        /**
         * Gets the appropriate drawable for subclasses to draw to,
         * When no window is currently in use (outside beforeDraw - afterDraw)
         * this returns (xcb_gcontext_t)-1.
         */
        inline xcb_drawable_t getDrawable(void) const {
            return bufferData.drawable;
        }
        
        /**
         * Gets the graphics context which is associated with the current window.
         * When no window is currently in use (outside beforeDraw - afterDraw)
         * this returns (xcb_gcontext_t)-1.
         * This is provided for convenience.
         */
        inline xcb_gcontext_t getGraphics(void) const {
            return bufferData.graphics;
        }
    
        // ------------------------ Theme implementation -----------------------
        
        // Methods we override to provide the required operations like double buffering.
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
        virtual void afterDraw(const rascUI::Rectangle & drawnArea) override;
    protected:
        virtual void destroy(void) override;
    public:
        
        /**
         * This returns a pointer to our BufferData. This contains all necessary
         * data for custom drawing.
         */
        virtual void * getDrawBuffer(void) override;
    
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        
        // Methods we override and do nothing, as to allow use in a ThemeGroup.
        virtual void redrawFromBuffer(void) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
        virtual void drawCheckboxButton(const rascUI::Location & location) override;
        virtual void drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const rascUI::Location & location) override;
        virtual void drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) override;
        virtual void drawWindowDecorationBackPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationFrontPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) override;
        
        virtual void customSetDrawColour(void * colour) override;
        virtual void * customGetTextColour(const rascUI::Location & location) override;
        virtual void customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) override;
    };
}

#endif
