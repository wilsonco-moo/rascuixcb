/*
 * Atom.cpp
 *
 *  Created on: 1 Aug 2024
 *      Author: wilson
 */

#include "Atom.h"

#include <iostream>
#include <cstddef>
#include <cstdlib>

namespace rascUIxcb {

    Atom::Atom(xcb_connection_t * connection, bool onlyIfExists, size_t nameLength, const char * name) :
        connection(connection),
        cookie(connection ? xcb_intern_atom(connection, onlyIfExists, nameLength, name) : xcb_intern_atom_cookie_t({ 0 })),
        atom(XCB_ATOM_NONE) {
        
        // Complain if xcb_intern_atom gave us a zero cookie (if connection is
        // valid).
        if (cookie.sequence == 0 && connection != NULL) {
            std::cerr << "WARNING: rascUIxcb::Atom: " << "xcb_intern_atom returned invalid cookie, will default to XCB_ATOM_NONE.\n";
        }
    }
    
    Atom::~Atom(void) {
        // Discard cookie if we've not read it yet, (else the reply won't ever
        // get freed by xcb when it shows up).
        if (cookie.sequence != 0) {
            xcb_discard_reply(connection, cookie.sequence);
        }
    }
    
    xcb_atom_t Atom::getAtom(void) {
        // If we've still got a valid cookie (i.e: not grabbed the reply yet),
        // ask xcb for a reply.
        if (cookie.sequence != 0) {
            xcb_intern_atom_reply_t * const reply = xcb_intern_atom_reply(connection, cookie, NULL);
            
            // Store the atom from the reply, complain if the reply failed.
            if (reply == NULL) {
                std::cerr << "WARNING: rascUIxcb::Atom: " << "xcb_intern_atom_reply returned an invalid reply, will default to XCB_ATOM_NONE.\n";
            } else {
                atom = reply->atom;
                free(reply);
            }
            
            // Clear the cookie now we've used it: We can used the cached atom
            // next time.
            cookie.sequence = 0;
        }
        
        return atom;
    }
}
