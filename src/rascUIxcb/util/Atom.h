/*
 * Atom.h
 *
 *  Created on: 1 Aug 2024
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_ATOM_H_
#define RASCUIXCB_UTIL_ATOM_H_

#include <xcb/xcb.h>

namespace rascUIxcb {

    /**
     * Wrapper to easily encapsulate an async request for an atom.
     * The request is sent out to the X server as soon as we're created, but
     * a reply is only required the first time getAtom is called.
     */
    class Atom {
    private:
        xcb_connection_t * connection;
        xcb_intern_atom_cookie_t cookie;
        xcb_atom_t atom;

    public:
        /**
         * Main constructor: Send off the request for the atom to the x server.
         * onlyIfExists: If true, only return a valid atom if it already exists.
         * Else, if false, create the atom if it doesn't exist already.
         */
        Atom(xcb_connection_t * connection, bool onlyIfExists, size_t nameLength, const char * name);
        ~Atom(void);
        
        /**
         * Get our associated atom, waiting for the request reply if needed.
         * If onlyIfExists was true on creation and the atom didn't exist,
         * returns XCB_ATOM_NONE. Else returns a valid atom.
         */
        xcb_atom_t getAtom(void);
    };
}

#endif
