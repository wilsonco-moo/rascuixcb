/*
 * Colour.cpp
 *
 *  Created on: 19 Jun 2019
 *      Author: wilson
 */

#include "Colour.h"

#include <xcb/xproto.h>
#include <iostream>
#include <cstddef>
#include <cstdlib>

#include "../platform/Context.h"

namespace rascUIxcb {
    
    Colour::Colour(uint16_t red, uint16_t green, uint16_t blue, Context * context) :
        channels{red, green, blue},
        receivedColourYet(false),
        colourSuccess(false),
        context(NULL) {
        setContext(context);
    }
    
    Colour::~Colour(void) {
        // Setting the context to NULL will free our colour from (any) existing context.
        setContext(NULL);
    }

    void Colour::setContext(Context * newContext) {
        // If the context has been set to the same as the original one, there is nothing to do.
        if (context == newContext) return;
        
        // First, get rid of the old context (if we had one).
        if (context != NULL) {
            // Make sure we are no longer waiting for the colour.
            get();
            
            // If we got the colour successfully, free it from the colour map.
            // Note that this won't interfere with other Colour instances using
            // the same colour, because the number of allocations/frees is counted up.
            if (colourSuccess) {
                xcb_free_colors(context->xcbGetConnection(), context->xcbGetScreen()->default_colormap, 0, 1, &colour);
            }
        }
        
        // Next, assign the new context, and if it is not NULL mark that we have not yet
        // received the colour, and allocate it.
        context = newContext;
        if (newContext != NULL) {
            receivedColourYet = false;
            cookie = xcb_alloc_color(newContext->xcbGetConnection(), newContext->xcbGetScreen()->default_colormap, channels[0], channels[1], channels[2]);
        }
    }

    uint32_t Colour::get(void) {
        
        // Complain if this was called before setting the context.
        if (context == NULL) {
            std::cerr << "WARNING: rascUIxcb::Colour: " << "get() method called with NULL Context.\n";
            return 0;
        }
        
        // If we have already received the colour, just return it.
        if (receivedColourYet) {
            return colour;
        }
        
        // Otherwise, get the reply from the cookie.
        xcb_alloc_color_reply_t * reply = xcb_alloc_color_reply(context->xcbGetConnection(), cookie, NULL);
        
        // If the getting the colour failed, complain, set success to false, and use a placeholder colour.
        if (reply == NULL) {
            std::cerr << "WARNING: rascUIxcb::Colour: " << "failed to lookup colour, will default to a placeholder colour of 0 instead.\n";
            colour = 0;
            colourSuccess = false;
        
        // Otherwise set the colour, set success to true and free the reply (since it is dynamically allocated).
        } else {
            colour = reply->pixel;
            colourSuccess = true;
            free(reply);
        }
        
        // Mark that we have received the colour and return it.
        receivedColourYet = true;
        return colour;
    }

    void Colour::setBackground(xcb_gcontext_t gc) {
        uint32_t col = get();
        xcb_change_gc(context->xcbGetConnection(), gc, XCB_GC_BACKGROUND, &col);
    }
    
    void Colour::setForeground(xcb_gcontext_t gc) {
        uint32_t col = get();
        xcb_change_gc(context->xcbGetConnection(), gc, XCB_GC_FOREGROUND, &col);
    }
}
