/*
 * Colour.h
 *
 *  Created on: 19 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_COLOUR_H_
#define RASCUIXCB_UTIL_COLOUR_H_

#include <xcb/xcb.h>
#include <cstdint>

namespace rascUIxcb {

    class Context;

    /**
     * This class represents a colour allocated within an XCB colour map.
     * Note that multiple Colour instances can allocate the same colour,
     * and they will be given the same "cell" in the X colour map.
     * That cell will only be freed once ALL Colours have freed it.
     *
     * NOTE: All colour instances created from a given Context MUST
     * be destroyed BEFORE the Context instance.
     */
    class Colour {
    private:
        // Our internal colour, set in the constructor.
        uint16_t channels[3];
        
        // Set to true once get has been called, and we have received our colour
        // from the server.
        bool receivedColourYet;
        
        // Once we have received our colour, this is set to true if getting
        // the colour was successful, and thus if we *need* to free it from
        // the colour map.
        bool colourSuccess;
        
        union {
            // The response cookie. This is used while waiting for the colour response.
            // (When receivedColourYet == false).
            xcb_alloc_color_cookie_t cookie;
            // The final colour.
            // (When receivedColourYet == true).
            uint32_t colour;
        };

        // A pointer to our relevant Context. We must keep a reference to this
        // so we can remove ourself from the colour map when no longer needed.
        Context * context;

        // Don't allow copying: We hold raw resources.
        Colour(const Colour & other);
        Colour & operator = (const Colour & other);

    public:
        /**
         * Creates a colour with a Context. If the Context is NULL, then
         * setContext must be called at some point later, BEFORE get is called.
         *
         * NOTE: Colour values are 16 bit (0-65535 inclusive), NOT 8 bit.
         */
        Colour(uint16_t red, uint16_t green, uint16_t blue, Context * context = NULL);

        ~Colour(void); // Don't make this virtual - it is not needed.

        /**
         * Allows the Context to be assigned (or changed).
         * Setting the context to NULL here will free our colour from the
         * colour map.
         */
        void setContext(Context * newContext);

        /**
         * setConnection (or the appropriate constructor) MUST have been called
         * before this is run.
         */
        uint32_t get(void);

        /**
         * Sets the foreground and background colours of a graphics context,
         * to this colour.
         */
        void setBackground(xcb_gcontext_t gc);
        void setForeground(xcb_gcontext_t gc);
        
        /**
         * These methods allow access to our internal RGB colour representation.
         */
        inline uint16_t getRed(void) const { return channels[0]; }
        inline uint16_t getGreen(void) const { return channels[1]; }
        inline uint16_t getBlue(void) const { return channels[2]; }
    };
}

#endif
