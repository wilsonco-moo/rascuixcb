/*
 * Connection.cpp
 *
 *  Created on: 10 Jun 2019
 *      Author: wilson
 */

#include "Connection.h"

#include <X11/Xlib-xcb.h>
#include <xcb/xcb_aux.h>
#include <xcb/xproto.h>
#include <sys/select.h>
#include <algorithm>
#include <unistd.h>
#include <iostream>
#include <cstdint>
#include <cstddef>
#include <fcntl.h>
#include <cctype>
#include <cerrno>
#include <ctime>

namespace rascUIxcb {

    Connection::Connection(const char * displayname) :
        nextEvent(NULL) {
        
        // Create the X connection using Xlib, then convert it to an xcb connection.
        // This allows use of an additional Xlib display, for compatibility.
        // Since we are primarily using xcb, set the owner of the event queue to be xcb.
        xlibDisplay = XOpenDisplay(displayname);
        if (xlibDisplay == NULL) {
            connection = NULL;
        } else {
            connection = XGetXCBConnection(xlibDisplay);
            XSetEventQueueOwner(xlibDisplay, XCBOwnsEventQueue);
        }
        
        // Try to open the pipe, and complain if doing so fails.
        int pipeFds[2];
        if (pipe2(pipeFds, O_NONBLOCK) != 0) {
            std::cerr << "WARNING: rascUIxcb::Connection: " << "Failed to initialise pipe.\n";
            readPipe = -1;
            writePipe = -1;
        } else {
            readPipe = pipeFds[0];
            writePipe = pipeFds[1];
        }
    }

    Connection::~Connection(void) {
        // Close the pipes, complain if doing so fails.
        if (close(readPipe) != 0) {
            std::cerr << "WARNING: rascUIxcb::Connection: " << "Failed to close input pipe.\n";
        }
        if (close(writePipe) != 0) {
            std::cerr << "WARNING: rascUIxcb::Connection: " << "Failed to close output pipe.\n";
        }
        
        // This must be done, and is safe to do, since this is always either NULL or not freed.
        free(nextEvent);
        // Close the connection to the X server. Do this with the xlib method, since we opened
        // the connection with xlib.
        if (xlibDisplay != NULL) {
            XCloseDisplay(xlibDisplay);
        }
    }
    
    bool Connection::connectionSuccessful(void) const {
        // Return true if there was no connection error, and the pipe did not fail to open (we didn't set it to -1).
        return xlibDisplay != NULL && connection != NULL && xcb_connection_has_error(connection) == 0 && readPipe != -1;
    }

    // Reads all the data in the pipe, until there is none left to read.
    // Returns true upon success (or if done nothing), and false upon failure.
    static bool emptyPipe(int readPipe) {
        while(true) {
            char c;
            ssize_t ret = read(readPipe, &c, 1);
            if (ret == -1) {
                if (errno == EAGAIN || errno == EWOULDBLOCK) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
    
    xcb_generic_event_t * Connection::pollForEventWithNext(void) {
        xcb_generic_event_t * returnEvent;
        if (nextEvent == NULL) {
            returnEvent = xcb_poll_for_event(connection);
            if (returnEvent != NULL) {
                nextEvent = xcb_poll_for_event(connection);
            }
        } else {
            returnEvent = nextEvent;
            nextEvent = xcb_poll_for_event(connection);
        }
        return returnEvent;
    }

    bool Connection::pollForCheckAndReturnEvent(xcb_generic_event_t ** event, bool * error) {

        // First poll for an event, assume no errors unless we run into any.
        *error = false;
        *event = pollForEventWithNext();

        // If an event got generated, empty the pipe, check connection errors.
        if (*event != NULL) {
            if (!emptyPipe(readPipe)) {
                std::cerr << "WARNING: rascUIxcb::Connection: " << "Pipe closed while X event still pending.\n";
                *error = true;
            }
            if (xcb_connection_has_error(connection) != 0) {
                std::cerr << "WARNING: rascUIxcb::Connection: " << "X connection generated event, but also closed due to errors.\n";
                *error = true;
            }
            return true;
        }

        // If we didn't produce an event, but there was an error, then set error and return.
        if (xcb_connection_has_error(connection) != 0) {
            std::cerr << "WARNING: rascUIxcb::Connection: " << "X connection closed due to error, without generating event.\n";
            *error = true;
            return true;
        }

        // Return false if we should not exit the getPipedEvent method, since nothing useful happened.
        return false;
    }


    void Connection::getPipedEvent(xcb_generic_event_t ** event, bool * error, unsigned long long * timeout) {

        // First poll for, check and try to return an event. This avoids expensive I/O operations if we
        // get many events all at once.
        // NOTE: This always sets both event and error to something.
        if (pollForCheckAndReturnEvent(event, error)) return;

        // Now we can assume there are no (immediate) errors, and there are no events waiting.
        // So get the X connection's file descriptor.
        int xConn = xcb_get_file_descriptor(connection);
        
        // If the timeout is NULL, set the timeout struct pointer to NULL.
        // Otherwise, convert the timeout value to a timespec.
        struct timespec * timeoutStruct,
                          timeoutStructValue;
        if (timeout == NULL) {
            timeoutStruct = NULL;
        } else {
            timeoutStruct = &timeoutStructValue;
            timeoutStructValue.tv_sec = (*timeout) / 1000;
            timeoutStructValue.tv_nsec = ((*timeout) % 1000) * 1000000;
        }
        
        // Calculate nfds and build file descriptor sets for read AND for error.
        int nfds = std::max(readPipe, xConn) + 1;
        fd_set fdSetRead, fdSetError;
        
        FD_ZERO(&fdSetRead);
        FD_SET(xConn, &fdSetRead);
        FD_SET(readPipe, &fdSetRead);
        
        FD_ZERO(&fdSetError);
        FD_SET(xConn, &fdSetError);
        FD_SET(readPipe, &fdSetError);
        
        // Now wait for the file descriptors with pselect.
        int ret = pselect(
            nfds,          // nfds - the maximum value of the file descriptors in all sets, plus one.
            &fdSetRead,    // File descriptors on which we want to wait for reading
            NULL,          // File descriptors on which we want to wait for writing
            &fdSetError,   // File descriptors on which we want to wait for errors
            timeoutStruct, // Timeout - as defined by the user, calculated earlier.
            NULL);         // Sigmask - NULL means don't change sigmask.

        // If pselect failed, then return because an event cannot possibly be
        // waiting. If error was EINTR (interrupt), don't set error unless emptying
        // pipe fails. Otherwise, set error.
        if (ret == -1) {
            if (errno == EINTR) {
                if (!emptyPipe(readPipe)) {
                    std::cerr << "WARNING: rascUIxcb::Connection: " << "Pipe closed while handling signal.\n";
                    *error = true;
                }
            } else {
                std::cerr << "WARNING: rascUIxcb::Connection: " << "Failed to wait for file descriptors.\n";
                *error = true;
            }
            return;
        }

        // If pselect was successful, then poll for, check and try to return an event.
        if (pollForCheckAndReturnEvent(event, error)) return;

        // Finally, if there were no errors, AND an event was NOT generated, someone MUST have written to
        // or closed the pipe, or pselect must have timed out. So empty the pipe, (check for errors in doing so), and return.
        if (!emptyPipe(readPipe)) {
            std::cerr << "WARNING: rascUIxcb::Connection: " << "Pipe closed.\n";
            *error = true;
        }
    }
    
    /*
    // Example code, for reference
    xcb_generic_event_t * waitForEventWithTimeout(xcb_connection_t * connection, const struct timespec & timeout) {
        xcb_generic_event_t * event = xcb_poll_for_event(connection);
        if (event != NULL || xcb_connection_has_error(connection) > 0) {
            return event;
        }
        int fd = xcb_get_file_descriptor(connection);
        fd_set fdSet;
        while (true) {
            FD_ZERO(&FileDescriptors);
            FD_SET(XCBFileDescriptor, &FileDescriptors);
            if (pselect(XCBFileDescriptor + 1, &FileDescriptors, nullptr, nullptr, &Timeout, nullptr) > 0)
            {
                if ((Event = xcb_poll_for_event(XConnection)))
                    break;
            }
        }
    }
    */
}
