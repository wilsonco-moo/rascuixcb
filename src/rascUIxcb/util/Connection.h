/*
 * Connection.h
 *
 *  Created on: 10 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_CONNECTION_H_
#define RASCUIXCB_UTIL_CONNECTION_H_

#include <X11/Xlib.h>
#include <xcb/xcb.h>
#include <cstddef>

namespace rascUIxcb {

    /**
     * This class encapsulates a connection, using XCB, to an X server.
     * The connection is opened when a Connection instance is created,
     * and automatically closed when the Connection instance is destroyed.
     * 
     * The purpose of Connection is simply to automate reading events,
     * while providing the capability to peek at new events, and providing
     * a pipe which, when written to, interrupts reading events.
     * 
     * An xlib Display is also kept, for compatability with Xlib functions.
     * This requires libx11-xcb-dev, and links to X11.
     */
    class Connection {
    private:
        // This stores the next event in the queue, so the user can peek at the next event.
        xcb_generic_event_t * nextEvent;
        
        // The Xlib Display - provided for compabibility. Using the xcb connection should
        // be perferred wherever possible.
        Display * xlibDisplay;
        
        // The XCB connection - accessible using getConnection.
        xcb_connection_t * connection;
        
        // The pipe file descriptors. Both are set to -1 if we fail to create the pipe.
        int readPipe, writePipe;
        
        // Don't allow copying: We hold X resources.
        Connection(const Connection & other);
        Connection & operator = (const Connection & other);
        
    public:
        /**
         * See xcb_connect and XOpenDisplay documentation for details of
         * displayname. If left as the default values, the default X display
         * will be used.
         */
        Connection(const char * displayname = NULL);
        ~Connection(void);

    private:
        // Acts exactly like xcb_poll_for_event - which returns either an event or NULL if none is available yet.
        // However, this version acts as a "conveyor belt": Whenever an event, after the one that this method
        // returns is also available, it gets put in nextEvent, so the user can peek at the next event.
        xcb_generic_event_t * pollForEventWithNext(void);
        
        // Used internally within getPipedEvent. Returns true if this should result in exiting the getPipedEvent method.
        //
        // This first polls for an event. If that returns something, we try to empty the pipe
        // and check for errors, and return this event, (and any errors).
        // If we get no events, we check the connection for errors, returning any errors which happen.
        //
        // event is always set to a valid event or NULL, error is always set to true or false.
        bool pollForCheckAndReturnEvent(xcb_generic_event_t ** event, bool * error);
    public:
        
        /**
         * Returns true if there have not yet been errors in the connection,
         * AND we successfully created our pipe.
         */
        bool connectionSuccessful(void) const;
        
        /**
         * Allows outside access to our connection.
         */
        inline xcb_connection_t * getConnection(void) const {
            return connection;
        }
        
        /**
         * Gets the file descriptor of the pipe which can be written to, to
         * interrupt waiting for events.
         */
        inline int getWritePipe(void) const {
            return writePipe;
        }
        
        /**
         * Tries to read an event from the connection. If an event is immediately available,
         * errors are checked, the pipe is emptied, and it is returned.
         * Otherwise, we wait until:
         *  > An event shows up, data is written to the pipe, or the (optional) timeout expires:
         *    In this case, we empty the pipe, check for errors and return an event, if available.
         *  > We are interrupted with EINTR:
         *    In this case, empty the pipe, check for errors and immediately return.
         *  > Reading from the connection or pipe fails:
         *    In this case, we return error.
         * 
         * Parameters:
         *  > "event" is ALWAYS set to either NULL, or an event which must be freed with free.
         *  > "error" is ALWAYS set to either true if an error happens on the pipe or connection, or false otherwise.
         *  > "timeout" can be specified as a number of milliseconds, or NULL for no (forever) timeout.
         * 
         * NOTE: It is possible for error to be set to true, even when a valid event is returned, in
         *       the case that an error happened after the event was generated.
         * NOTE: If a non NULL event is returned it must be freed with free.
         */
        void getPipedEvent(xcb_generic_event_t ** event, bool * error, unsigned long long * timeout = NULL);

        /**
         * Allows access to the next waiting event, i.e: The one which will be returned by the next iteration
         * of getPipedEvent, if such an event exists. If there are no pending events after the one just returned
         * by getPipedEvent, this returns NULL.
         *
         * NOTE: The caller must NOT free the event returned by this method, as it will be later returned by
         *       getPipedEvent.
         */
        inline xcb_generic_event_t * peekNextEvent(void) const {
            return nextEvent;
        }
        
        /**
         * Allows access to our Xlib display. This is provided for compatibility
         * with Xlib functions: the xcb connection from getConnection should
         * always be preferred.
         */
        inline Display * getXlibDisplay(void) const {
            return xlibDisplay;
        }
    };
}

#endif
