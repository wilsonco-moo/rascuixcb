/*
 * KeySymbols.cpp
 *
 *  Created on: 21 Sep 2021
 *      Author: wilson
 */

#include "KeySymbols.h"

#define XK_MISCELLANY

#include <X11/keysymdef.h>
#include <cstddef>
#include <cctype>

#include "Connection.h"

namespace rascUIxcb {

    KeySymbols::KeySymbols(xcb_connection_t * connection) :
        keySymbols(xcb_key_symbols_alloc(connection)) {
    }
    
    KeySymbols::~KeySymbols(void) {
        // Free the key symbols when we're destroyed.
        xcb_key_symbols_free(keySymbols);
    }
    
    bool KeySymbols::wasSuccessful(void) const {
        return keySymbols != NULL;
    }
    
    xcb_keysym_t KeySymbols::keyCodeToKeySym(xcb_keycode_t keyCode, uint16_t state) const {
        
        #define ALT_GR_OFFSET 4
        
        // Get initial col value depending on the value of the shift mod mask.
        int col = ((state & XCB_MOD_MASK_SHIFT) == 0) ? 0 : 1;

        // If ALT GR mod mask is pressed, add the ALT GR offset.
        if ((state & 128) != 0) {
            col += ALT_GR_OFFSET;
        }

        // Get initial key sym.
        xcb_keysym_t keySym = xcb_key_symbols_get_keysym(keySymbols, keyCode, col);

        // Try swapping first mod mask if we get XCB_NO_SYMBOL.
        if (keySym == XCB_NO_SYMBOL) {
            keySym = xcb_key_symbols_get_keysym(keySymbols, keyCode, col ^ 1);
        }

        // If the caps lock is down and we have a printable ASCII character, swap the case
        if ((state & XCB_MOD_MASK_LOCK) != 0 && keySym <= 0x7f && std::isprint(keySym)) {
            if (std::isupper(keySym)) {
                keySym = std::tolower(keySym);
            } else {
                keySym = std::toupper(keySym);
            }
        }
        
        // If num lock is down, translate numpad keys to numpad numbers.
        if ((state & XCB_MOD_MASK_2) != 0) {
            switch(keySym) {
                case XK_KP_Insert:    keySym = XK_KP_0; break;
                case XK_KP_End:       keySym = XK_KP_1; break;
                case XK_KP_Down:      keySym = XK_KP_2; break;
                case XK_KP_Page_Down: keySym = XK_KP_3; break;
                case XK_KP_Left:      keySym = XK_KP_4; break;
                case XK_KP_Begin:     keySym = XK_KP_5; break;
                case XK_KP_Right:     keySym = XK_KP_6; break;
                case XK_KP_Home:      keySym = XK_KP_7; break;
                case XK_KP_Up:        keySym = XK_KP_8; break;
                case XK_KP_Page_Up:   keySym = XK_KP_9; break;
                case XK_KP_Delete:    keySym = XK_KP_Decimal; break;
            }
        }
        
        return keySym;
    }
    
    xcb_keycode_t * KeySymbols::keySymToKeyCodes(xcb_keysym_t keySym) const {
        return xcb_key_symbols_get_keycode(keySymbols, keySym);
    }
}
