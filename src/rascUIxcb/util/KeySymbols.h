/*
 * KeySymbols.h
 *
 *  Created on: 21 Sep 2021
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_KEYSYMBOLS_H_
#define RASCUIXCB_UTIL_KEYSYMBOLS_H_

#include <xcb/xcb_keysyms.h>
#include <xcb/xcb.h>

namespace rascUIxcb {
    class Connection;

    /**
     * Utility class encapsulating a table of X key symbols. This is required
     * for converting between keycodes and keysyms.
     * We allocate and free a table of key symbols when we're created/destroyed.
     */
    class KeySymbols {
    private:
        // A table of key symbols, required for converting keycodes to keysyms.
        xcb_key_symbols_t * keySymbols;

        // Don't allow copying: We hold X resources.
        KeySymbols(const KeySymbols & other);
        KeySymbols & operator = (const KeySymbols & other);

    public:
        /**
         * Creating requires a connection from which to allocate the key
         * symbols.
         */
        KeySymbols(xcb_connection_t * connection);
        ~KeySymbols(void);
        
        /**
         * Allows access to our internal key symbols table.
         */
        inline xcb_key_symbols_t * getTable(void) const {
            return keySymbols;
        }
        
        /**
         * Returns true if our key symbols table loaded successfully.
         */
        bool wasSuccessful(void) const;
        
        /**
         * Converts a keycode to a keysym, using information from key state.
         * For example, from a xcb_key_press_event_t, key code is the "detail"
         * member, and key state is the "state" member.
         *
         * Adapted from the following, lines 916 - 958.
         *  - Original: https://cep.xray.aps.anl.gov/software/qt4-x11-4.8.6-browser/d1/da0/qxcbkeyboard_8cpp_source.html
         *  - Archive:  https://web.archive.org/web/20180326040020/https://cep.xray.aps.anl.gov/software/qt4-x11-4.8.6-browser/d1/da0/qxcbkeyboard_8cpp_source.html
         */
        xcb_keysym_t keyCodeToKeySym(xcb_keycode_t keyCode, uint16_t state) const;
        
        /**
         * Convenient wrapper around xcb_key_symbols_get_keycode: returns a list
         * of key codes which map to the specified key sym, or NULL if no key
         * codes match. The list of key codes is terminated with XCB_NO_SYMBOL.
         * Note: This must be freed with free.
         * 
         * Documentation and source for xcb_key_symbols_get_keycode can be found at:
         * Original:
         *   https://code.woboq.org/qt5/include/xcb/xcb_keysyms.h.html
         *   https://code.woboq.org/qt5/qtbase/src/3rdparty/xcb/xcb-util-keysyms/keysyms.c.html
         * Archive:
         *   http://web.archive.org/web/20200815161533/https://code.woboq.org/qt5/include/xcb/xcb_keysyms.h.html
         *   http://web.archive.org/web/20210921205244/https://code.woboq.org/qt5/qtbase/src/3rdparty/xcb/xcb-util-keysyms/keysyms.c.html
         */
        xcb_keycode_t * keySymToKeyCodes(xcb_keysym_t keySym) const;
    };
}

#endif
