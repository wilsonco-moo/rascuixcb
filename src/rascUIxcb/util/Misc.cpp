/*
 * Misc.cpp
 *
 *  Created on: 02 Jan 2025
 *      Author: wilson
 */

#include "Misc.h"

namespace rascUIxcb {
    rascUI::Rectangle Misc::xcbToRascUIRect(const xcb_rectangle_t & xcbRectangle) {
        return rascUI::Rectangle(xcbRectangle.x, xcbRectangle.y, xcbRectangle.width, xcbRectangle.height);
    }

    xcb_rectangle_t Misc::rascUIToXcbRect(const rascUI::Rectangle & rascUIRectangle, unsigned int minSize) {
        xcb_rectangle_t xcbRectangle;
        xcbRectangle.x = clampToPureInt16(rascUIRectangle.x);
        xcbRectangle.y = clampToPureInt16(rascUIRectangle.y);
        xcbRectangle.width = clampToPureSize16(rascUIRectangle.width, minSize);
        xcbRectangle.height = clampToPureSize16(rascUIRectangle.height, minSize);
        return xcbRectangle;
    }
}
