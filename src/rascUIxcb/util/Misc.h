/*
 * Misc.h
 *
 *  Created on: 24 Sep 2021
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_MISC_H_
#define RASCUIXCB_UTIL_MISC_H_

#include <rascUI/util/Rectangle.h>
#include <xcb/xcb_util.h>
#include <xcb/xcb.h>
#include <cstdint>

/**
 * Header file providing miscellaneous util macros and other methods, for things
 * like dealing with xcb events more conveniently, and other strange xcb things.
 * All macros here are prefixed with RASCUIXCB_, methods are in rascUIxcb::Misc.
 */

/**
 * Peeks at the next event from the specified (reference to a) rascUIxcb
 * Connection, and returns true if the next event exists, AND is of the
 * specified event type.
 * Note: get event type using macro from xcb util (xcb_event.h), see
 * http://web.archive.org/web/20101224073645/http://xcb.freedesktop.org/XcbUtil/api/group__xcb____event__t.html
 */
#define RASCUIXCB_NEXT_EVENT_IS(xcbConnection, eventType)                       \
    ((xcbConnection).peekNextEvent() != NULL &&                                 \
     XCB_EVENT_RESPONSE_TYPE((xcbConnection).peekNextEvent()) == (eventType))

/**
 * Peeks at the next event from the specified (reference to a) rascUIxcb
 * Connection, and returns true if either the next event doesn't exist,
 * OR doesn't have the specified event type.
 */
#define RASCUIXCB_NEXT_EVENT_IS_NOT(xcbConnection, eventType)                   \
    ((xcbConnection).peekNextEvent() == NULL ||                                 \
     XCB_EVENT_RESPONSE_TYPE((xcbConnection).peekNextEvent()) != (eventType))

/**
 * Same as RASCUIXCB_NEXT_EVENT_IS, but will only return true if the event is
 * ALSO related to the same window as specified by "searchWindow".
 * When managing multiple windows, this is preferred over
 * RASCUIXCB_NEXT_EVENT_IS, (else we can skip chains of events with interleaved
 * window ids).
 */
#define RASCUIXCB_NEXT_EVENT_IS_WINDOW(xcbConnection, eventType, eventStructureType, eventStructureWindowMember, searchWindow) \
    ((xcbConnection).peekNextEvent() != NULL &&                                                                                \
     XCB_EVENT_RESPONSE_TYPE((xcbConnection).peekNextEvent()) == (eventType) &&                                                \
     ((eventStructureType *)(xcbConnection).peekNextEvent())->eventStructureWindowMember == (searchWindow))

/**
 * Same as RASCUIXCB_NEXT_EVENT_IS_NOT, but ALSO returns true when the next
 * event is related to a different window than specified by "searchWindow".
 * When managing multiple windows, this is preferred over
 * RASCUIXCB_NEXT_EVENT_IS_NOT, (else we can skip chains of events with
 * interleaved window ids).
 */
#define RASCUIXCB_NEXT_EVENT_IS_NOT_WINDOW(xcbConnection, eventType, eventStructureType, eventStructureWindowMember, searchWindow) \
    ((xcbConnection).peekNextEvent() == NULL ||                                                                                    \
     XCB_EVENT_RESPONSE_TYPE((xcbConnection).peekNextEvent()) != (eventType) ||                                                    \
     ((eventStructureType *)(xcbConnection).peekNextEvent())->eventStructureWindowMember != (searchWindow))

namespace rascUIxcb {
    
    /**
     * Class containing any miscellaneous util methods.
     */
    class Misc {
    public:
        /**
         * Default minimum size which things can be resized to, without x11
         * reporting errors. Used as a default minimum size in the methods
         * below.
         */
        constexpr static unsigned int MIN_CLAMP_SIZE = 1;
        
        /**
         * Clamps the specified value to the range of a 16 bit signed int, then
         * converts it to a uint32_t. Input types must be signed, and can be
         * floats.
         * This is required for generating input x/y values for
         * xcb_configure_window, where they're 16 bit signed integers, stored
         * inside a 32 bit value.
         * See: https://x.org/releases/X11R7.7/doc/xproto/x11protocol.html#requests:ConfigureWindow
         * http://web.archive.org/web/20240704125954/https://x.org/releases/X11R7.7/doc/xproto/x11protocol.html#requests:ConfigureWindow
         */
        template<typename Type>
        static uint32_t clampToInt16(Type value);
        
        /**
         * Clamps an int32_t value to the range of a 16 bit unsigned int,
         * ensuring it isn't less than minSize, then converts it to a uint32_t.
         * Input types must be unsigned or floats.
         * This is required for generating input size values for
         * xcb_configure_window, where they're 16 bit unsigned integers, stored
         * inside a 32 bit value. X11 also typically doesn't like things resized
         * smaller than MIN_CLAMP_SIZE (1).
         * See: https://x.org/releases/X11R7.7/doc/xproto/x11protocol.html#requests:ConfigureWindow
         * http://web.archive.org/web/20240704125954/https://x.org/releases/X11R7.7/doc/xproto/x11protocol.html#requests:ConfigureWindow
         */
        template<typename Type>
        static uint32_t clampToSize16(Type value, unsigned int minSize = MIN_CLAMP_SIZE);
        
        /**
         * Same as clampToInt16, but without the cast to a uint32_t. Helpful
         * for setting sizes in various xcb structures (like configure notify
         * events.
         */
        template<typename Type>
        static int16_t clampToPureInt16(Type value);
        
        /**
         * Same as clampToSize16, but without the cast to a uint32_t. Helpful
         * for setting sizes in various xcb structures (like configure notify
         * events.
         */
        template<typename Type>
        static uint16_t clampToPureSize16(Type value, unsigned int minSize = MIN_CLAMP_SIZE);
        
        /**
         * Convert an xcb rectangle to a rascUI rectangle.
         */
        static rascUI::Rectangle xcbToRascUIRect(const xcb_rectangle_t & xcbRectangle);
        
        /**
         * Convert a rascUI rectangle to an xcb rectangle, clamping all values.
         */
        static xcb_rectangle_t rascUIToXcbRect(const rascUI::Rectangle & rascUIRectangle, unsigned int minSize = MIN_CLAMP_SIZE);
    };
}

#include "Misc.inl"

#endif
