/*
 * Misc.inl
 *
 *  Created on: 23 Jul 2024
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_MISC_INL_
#define RASCUIXCB_UTIL_MISC_INL_

#include "Misc.h"

#include <type_traits>
#include <algorithm>
#include <limits>

namespace rascUIxcb {
    
    template<typename Type>
    uint32_t Misc::clampToInt16(Type value) {
        return clampToPureInt16(value);
    }
    
    template<typename Type>
    uint32_t Misc::clampToSize16(Type value, unsigned int minSize) {
        return clampToPureSize16(value, minSize);
    }
    
    template<typename Type>
    int16_t Misc::clampToPureInt16(Type value) {
        // Sanity check input type.
        static_assert(std::is_signed<Type>::value, "clampToInt16 input type must be signed!");
        static_assert(sizeof(Type) >= sizeof(int16_t), "clampToInt16 input type must be at least a 16 bit int");
        
        // Clamp to range of int16_t (min/max on input type), then convert to
        // int16_t. This way floats are handled properly too.
        return (int16_t)std::max<Type>(std::min<Type>(value, std::numeric_limits<int16_t>::max()), std::numeric_limits<int16_t>::min());
    }

    template<typename Type>
    uint16_t Misc::clampToPureSize16(Type value, unsigned int minSize) {
        // Sanity check input type.
        static_assert(std::is_unsigned<Type>::value ? sizeof(Type) >= sizeof(uint16_t) : sizeof(Type) > sizeof(uint16_t), "clampToInt16 input type must be at least a 16 bit unsigned int, or if signed, must be large enough to store uint16_t max.");
        
        // Clamp to range of uint16_t with min size (min/max on input type),
        // then convert to uint16_t. This way floats are handled properly too.
        return (uint16_t)std::max<Type>(std::min<Type>((value), std::numeric_limits<uint16_t>::max()), minSize);
    }
}

#endif
