/*
 * ScreenConfig.cpp
 *
 *  Created on: 29 Dec 2024
 *      Author: wilson
 */

#include "ScreenConfig.h"

#include <algorithm>
#include <iostream>
#include <cstddef>
#include <cstdlib>

#include "../platform/Context.h"
#include "Misc.h"

namespace rascUIxcb {

    ActiveCrtc::ActiveCrtc(const xcb_rectangle_t & dimensions) :
        dimensions(dimensions),
        borderedDimensions(dimensions) {
    }
    
    void ActiveCrtc::place(xcb_rectangle_t & rectangle) const {
        // Find our centre.
        const int centreX = borderedDimensions.x + (borderedDimensions.width / 2);
        const int centreY = borderedDimensions.y + (borderedDimensions.height / 2);
        
        // Set position so rectangle is centred, make sure top-left corner is
        // visible.
        rectangle.x = Misc::clampToPureInt16(std::max<int>(borderedDimensions.x, centreX - (rectangle.width / 2)));
        rectangle.y = Misc::clampToPureInt16(std::max<int>(borderedDimensions.y, centreY - (rectangle.height / 2)));
    }

    ScreenConfig::ScreenConfig(Context & context) :
        context(context),
        resourcesCookie({ 0 }),
        screenInfoCookie({ 0 }),
        extensionCookie({ 0 }),
        randrExtensionPresent(false),
        randrBaseEventId(0) {
        
        // Grab screen and connection (null-check as connection could've
        // failed).
        xcb_screen_t * const screen = context.xcbGetScreen();
        xcb_connection_t * const connection = context.xcbGetConnection();
        if (screen != NULL && connection != NULL) {
            
            // According to xdpyinfo -queryExtensions, xrandr extension is just
            // called "RANDR". (name_len, name). Go ahead and just assume randr
            // is present *before* we get the response - all that can happen is
            // for our requests to be refused. We only want this for the base
            // event id, any sensible X server is gonna have this.
            extensionCookie = xcb_query_extension(connection, 5, "RANDR");
            
            // We assume there's only one screen (root), as I don't know of any
            // case where there are multiple! Nowadays multiple displays are
            // done by mapping multiple CRTCs to one root screen!
            
            // Request to be notified of any changes to CRTCs. In response we'll
            // re-request screen resources.
            // See: https://github.com/qt/qtbase/blob/dev/src/plugins/platforms/xcb/qxcbconnection_screens.cpp
            // https://web.archive.org/web/20241231152321/https://raw.githubusercontent.com/qt/qtbase/refs/heads/dev/src/plugins/platforms/xcb/qxcbconnection_screens.cpp
            xcb_randr_select_input(connection, screen->root, XCB_RANDR_NOTIFY_MASK_CRTC_CHANGE);
            
            // Initially grab screen resources.
            // Use xcb_randr_get_screen_resources_current not 
            // xcb_randr_get_screen_resources! Otherwise the X server polls
            // hardware for the response (which takes ages), and freezes up the
            // whole X server for a second or so!
            // See: https://forums.developer.nvidia.com/t/xrandr-slowdown-solved-with-current/317492
            // http://web.archive.org/web/20241231221850/https://forums.developer.nvidia.com/t/xrandr-slowdown-solved-with-current/317492
            // Also: https://github.com/glfw/glfw/issues/347
            // http://web.archive.org/web/20201030172622/https://github.com/glfw/glfw/issues/347
            resourcesCookie = xcb_randr_get_screen_resources_current(connection, screen->root);
            
            // Also request screen info. We'll use this as a fallback if
            // screen resources tells us that there's no CRTCs. That
            // happens in Xephyr (test environment) and with borked
            // graphics drivers on obscure hardware.
            screenInfoCookie = xcb_randr_get_screen_info(connection, screen->root);
        }
    }
    
    ScreenConfig::~ScreenConfig(void) {
        discardCrtcCookiesData();
        
        // Discard other cookies (which aren't for CRTC stuff).
        xcb_connection_t * const connection = context.xcbGetConnection();
        if (extensionCookie.sequence != 0) {
            xcb_discard_reply(connection, extensionCookie.sequence);
        }
    }
    
    void ScreenConfig::discardCrtcCookiesData(void) {
        xcb_connection_t * const connection = context.xcbGetConnection();
        
        if (resourcesCookie.sequence != 0) {
            xcb_discard_reply(connection, resourcesCookie.sequence);
            resourcesCookie.sequence = 0;
        }
        
        if (screenInfoCookie.sequence != 0) {
            xcb_discard_reply(connection, screenInfoCookie.sequence);
            screenInfoCookie.sequence = 0;
        }
        
        for (xcb_randr_get_crtc_info_cookie_t crtcCookie : crtcCookies) {
            if (crtcCookie.sequence != 0) {
                xcb_discard_reply(connection, crtcCookie.sequence);
            }
        }
        crtcCookies.clear();
        // Also discard cached CRTC data.
        activeCrtcs.clear();
    }
    
    bool ScreenConfig::receiveCrtcs(void) {
        bool returnStatus = true;
        
        // If there are crtc cookies, grab reply for each one.
        if (!crtcCookies.empty()) {
            for (const xcb_randr_get_crtc_info_cookie_t crtcInfoCookie : crtcCookies) {
                xcb_randr_get_crtc_info_reply_t * const crtcInfoReply = xcb_randr_get_crtc_info_reply(context.xcbGetConnection(), crtcInfoCookie, NULL);
                if (crtcInfoReply == NULL) {
                    std::cerr << "rascUIxcb::ScreenConfig: " << "Failed to receive CRTC info, ignoring.\n";
                } else {
                    // Only read ones with successful status.
                    if (crtcInfoReply->status == XCB_RANDR_SET_CONFIG_SUCCESS) {
                        // If this CRTC has an output connected to it, we consider
                        // it to be "active" so add it to our list. Else ignore it.
                        if (xcb_randr_get_crtc_info_outputs_length(crtcInfoReply) > 0) {
                            xcb_rectangle_t rectangle;
                            rectangle.x = crtcInfoReply->x;
                            rectangle.y = crtcInfoReply->y;
                            rectangle.width = crtcInfoReply->width;
                            rectangle.height = crtcInfoReply->height;
                            activeCrtcs.emplace_back(rectangle);
                            //std::cout << "Found " << rectangle.x << ", " << rectangle.y << ", " << rectangle.width << ", " << rectangle.height << '\n';
                        }
                        
                    } else {
                        // If status is invalid, ignore this one. That can happen if
                        // configuration changes between our requests (race
                        // condition).
                        // See: https://gitlab.freedesktop.org/xorg/proto/xorgproto/-/raw/master/randrproto.txt?ref_type=heads
                        // Archive: http://web.archive.org/web/20241229134546/https://gitlab.freedesktop.org/xorg/proto/xorgproto/-/raw/master/randrproto.txt?ref_type=heads
                        std::cerr << "rascUIxcb::ScreenConfig: " << "Received CRTC info with failed status (" << (unsigned int)crtcInfoReply->status << "), retrying.\n";
                        returnStatus = false;
                    }
                    
                    free(crtcInfoReply);
                }
            }
            // We've consumed all cookies now, so clear them.
            crtcCookies.clear();
        }
        
        return returnStatus;
    }
    
    void ScreenConfig::requestScreenResources(void) {
        // Discard any previous requests.
        discardCrtcCookiesData();
        
        // Request resources and screen info, store cookies. Use
        // "_current" version, see comments in constructor.
        resourcesCookie = xcb_randr_get_screen_resources_current(context.xcbGetConnection(), context.xcbGetScreen()->root);
        screenInfoCookie = xcb_randr_get_screen_info(context.xcbGetConnection(), context.xcbGetScreen()->root);
    }
    
    void ScreenConfig::requestCrtcs(void) {
        // Do nothing unless there is a valid request for screen resources.
        if (resourcesCookie.sequence != 0) {
            xcb_connection_t * const connection = context.xcbGetConnection();
        
            // Grab screen resources reply.
            xcb_randr_get_screen_resources_current_reply_t * const screenResourcesReply = xcb_randr_get_screen_resources_current_reply(connection, resourcesCookie, NULL);
            
            // Work out whether we've been given valid CRTC data.
            bool bSuccess = true;
            int crtcCount;
            if (screenResourcesReply == NULL) {
                std::cerr << "rascUIxcb::ScreenConfig: " << "Failed to request screen resources, falling back to root screen info.\n";
                bSuccess = false;
            } else {
                crtcCount = xcb_randr_get_screen_resources_current_crtcs_length(screenResourcesReply);
                if (crtcCount == 0) {
                    std::cerr << "rascUIxcb::ScreenConfig: " << "Warning: There are zero CRTCs reported by RANDR. Assuming knackered graphics drivers or bad X server configuration, and falling back to root screen info.\n";
                    bSuccess = false;
                }
            }
            
            if (bSuccess) {
                // Successfully got CRTC data: Request info for each
                // one.
                const xcb_randr_crtc_t * crtcs = xcb_randr_get_screen_resources_current_crtcs(screenResourcesReply);
                crtcCookies.reserve(crtcCount);
                for (int index = 0; index < crtcCount; index++) {
                    crtcCookies.push_back(xcb_randr_get_crtc_info(connection, crtcs[index], screenResourcesReply->config_timestamp));
                }
                
                // We won't need screen info as a fallback, so just discard the reply.
                if (screenInfoCookie.sequence != 0) {
                    xcb_discard_reply(connection, screenInfoCookie.sequence);
                    screenInfoCookie.sequence = 0;
                }
                
            } else {
                // Failed to get CRTC info: Try falling back to screen info (assume this is
                // a broken configuration with a single screen/display/whatever).
                // If even that fails, we can just leave the list of active CRTCs blank
                // and assume there's none.
                xcb_randr_get_screen_info_reply_t * const screenInfoReply = xcb_randr_get_screen_info_reply(connection, screenInfoCookie, NULL);
                if (screenInfoReply == NULL) {
                    std::cerr << "rascUIxcb::ScreenConfig: " << "Failed to get screen info as a fallback, giving up and assuming zero CRTCs.\n";
                } else {
                    // We must find the size specified by the screen info's size id.
                    const int sizesCount = xcb_randr_get_screen_info_sizes_length(screenInfoReply);
                    if (screenInfoReply->sizeID < sizesCount) {
                        // Also sanity-check that the size makes sense!
                        const xcb_randr_screen_size_t & screenSize = xcb_randr_get_screen_info_sizes(screenInfoReply)[screenInfoReply->sizeID];
                        if (screenSize.width > 0 && screenSize.width > 0) {
                            // Add as a single "fake" entry to active CRTCs.
                            std::cerr << "rascUIxcb::ScreenConfig: " << "Falling back to screen info size of " << screenSize.width << 'x' << screenSize.height << " as a single \"fake CRTC\".\n";
                            xcb_rectangle_t rectangle;
                            rectangle.x = 0;
                            rectangle.y = 0;
                            rectangle.width = screenSize.width;
                            rectangle.height = screenSize.height;
                            activeCrtcs.emplace_back(rectangle);
                        } else {
                            std::cerr << "rascUIxcb::ScreenConfig: " << "Screen info fallback reported zero screen size, giving up and assuming zero CRTCs.\n";
                        }
                    } else {
                        std::cerr << "rascUIxcb::ScreenConfig: " << "Screen info fallback reported invalid or missing screen size, giving up and assuming zero CRTCs.\n";
                    }
                    
                    free(screenInfoReply);
                }
            }
            
            // Free reply and clear resources cookie now we're done.
            free(screenResourcesReply);
            resourcesCookie.sequence = 0;
        }
    }
    
    const std::vector<ActiveCrtc> & ScreenConfig::getActiveCrtcs(void) {
        
        while(true) {
            // Try receiving screen resources and requesting CRTC info. Will do
            // nothing unless we just sent out a request for screen resources.
            requestCrtcs();
            
            // Then, try receiving CRTC info - will do nothing unless we just
            // sent out a request for CRTC info. This can fail due to
            // configurarion changing in the meantime: If it does, re-request
            // screen resources and retry.
            if (receiveCrtcs()) {
                break;
            } else {
                requestScreenResources();
            }
        }
        
        return activeCrtcs;
    }
    
    bool ScreenConfig::hasRandrExtension(uint8_t & baseEventId) {
        // Receive response to extension query if not done yet.
        if (extensionCookie.sequence != 0) {
            xcb_query_extension_reply_t * const extensionReply = xcb_query_extension_reply(context.xcbGetConnection(), extensionCookie, NULL);
            if (extensionReply != NULL) {
                randrExtensionPresent = extensionReply->present;
                randrBaseEventId = extensionReply->first_event;
                free(extensionReply);
            }
            extensionCookie.sequence = 0;
        }
        
        // Return stored values.
        baseEventId = randrBaseEventId;
        return randrExtensionPresent;
    }
    
    const ActiveCrtc * ScreenConfig::getActiveCrtcUnderPointer(void) {
        // Make sure we have active CRTC data.
        getActiveCrtcs();
        
        if (activeCrtcs.empty()) {
            // None to choose from, return null.
            return NULL;
        } else if (activeCrtcs.size() == 1) {
            // Only one to choose from: pick the first one.
            return &activeCrtcs.front();
        } else {
            // Else we've got to wait for a round trip, unfortunately.
            // Query for pointer position.
            const xcb_query_pointer_cookie_t pointerCookie = xcb_query_pointer(context.xcbGetConnection(), context.xcbGetScreen()->root);
            xcb_query_pointer_reply_t * const pointerReply = xcb_query_pointer_reply(context.xcbGetConnection(), pointerCookie, NULL);
            
            // Pick the first one by default.
            const ActiveCrtc * result = &activeCrtcs.front();
            
            // Search CRTCs for one which contains the pointer position.
            if (pointerReply != NULL) {
                for (const ActiveCrtc & activeCrtc : activeCrtcs) {
                    if (pointerReply->root_x >= activeCrtc.dimensions.x &&
                        pointerReply->root_y >= activeCrtc.dimensions.y &&
                        pointerReply->root_x < activeCrtc.dimensions.x + activeCrtc.dimensions.width &&
                        pointerReply->root_y < activeCrtc.dimensions.y + activeCrtc.dimensions.height) {
                        result = &activeCrtc;
                    }
                }
                free(pointerReply);
            }
            
            return result;
        }
    }
}
