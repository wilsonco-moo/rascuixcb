/*
 * ScreenConfig.h
 *
 *  Created on: 29 Dec 2024
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_SCREENCONFIG_H_
#define RASCUIXCB_UTIL_SCREENCONFIG_H_

#include <xcb/randr.h>
#include <xcb/xcb.h>
#include <cstdint>
#include <vector>

namespace rascUIxcb {
    class Context;

    /**
     * Represents an active CRTC (see below).
     * "dimensions" represents the full (real) dimensions of the CRTC.
     * "borderedDimensions" is used for window placement: It can be overridden
     * by a window manager (etc) to account for panels placed at the edges of
     * the screen.
     */
    class ActiveCrtc {
    public:
        xcb_rectangle_t dimensions;
        xcb_rectangle_t borderedDimensions;
    
        /**
         * By default dimensions and bordered dimensions are set identically.
         */
        explicit ActiveCrtc(const xcb_rectangle_t & dimensions);
        
        /**
         * Sets x and y coordinates of the specified rectangle so it fits nicely
         * (centred) within our bordered dimensions.
         * We also make sure top-left corner is visible within bordered
         * dimensions.
         */
        void place(xcb_rectangle_t & rectangle) const;
    };

    /**
     * Queries and caches config of screen resources available to the X server.
     * Configuration is updated (re-queried) when xrandr notifies us of changes.
     * 
     * That is, we take note of positions and dimensions of each "active" CRTC,
     * (i.e: one assigned to an output. This is what's important if you're
     * considering window placement or snapping.
     * 
     * For how randr works, see:
     * https://gitlab.freedesktop.org/xorg/proto/xorgproto/-/blob/master/randrproto.txt?ref_type=heads
     * http://web.archive.org/web/20241229134546/https://gitlab.freedesktop.org/xorg/proto/xorgproto/-/raw/master/randrproto.txt?ref_type=heads
     */
    class ScreenConfig {
    private:
        Context & context;

        // Cookies and stored active CRTCs.
        xcb_randr_get_screen_resources_current_cookie_t resourcesCookie;
        xcb_randr_get_screen_info_cookie_t screenInfoCookie;
        std::vector<xcb_randr_get_crtc_info_cookie_t> crtcCookies;
        std::vector<ActiveCrtc> activeCrtcs;
        
        // Randr extension request and response.
        xcb_query_extension_cookie_t extensionCookie;
        bool randrExtensionPresent;
        uint8_t randrBaseEventId;
        
    public:
        ScreenConfig(Context & context);
        ~ScreenConfig(void);
        
    private:
        // Discards all screen resources and CRTC cookies, and stored CRTC data.
        void discardCrtcCookiesData(void);
        // Reads results of previous CRTC info requests, returns false if any
        // failed due to timestamp.
        // (Initialisation step 3, does nothing unless step 2 has just happened).
        bool receiveCrtcs(void);
    
    public:
        /**
         * Re-requests screen resources and clears all cached data. Call
         * whenever we're notified about changes to display configuration.
         * (Initialisation step 1, happens by default in constructor)
         */
        void requestScreenResources(void);
        
        /**
         * Reads a previous screen resources request, requests info about all
         * CRTCs. Call at a convenient time during initialisation to reduce
         * round-trip times!
         * (Initialisation step 2, does nothing unless step 1 has just happened).
         */
        void requestCrtcs(void);
        
        /**
         * Gets a list of dimensions of all active CRTCs (that is, those with
         * outputs assigned to them). These represent the dimensions of outputs
         * which users will see, in respect to the root window.
         * 
         * This function sends/receives requests as necessary to generate this
         * data.
         */
        const std::vector<ActiveCrtc> & getActiveCrtcs(void);
        
        /**
         * Returns true if the randr extension is enabled on the x server. If
         * so, also gets base event id for randr events.
         */
        bool hasRandrExtension(uint8_t & baseEventId);
        
        /**
         * Gets the active CRTC which is under the mouse pointer.
         * If there are multiple CRTCs to choose from, this takes a round-trip
         * to query for the mouse pointer position!
         * Returns null if we have no active CRTCs.
         * Returns the first one if none contain the pointer position.
         */
        const ActiveCrtc * getActiveCrtcUnderPointer(void);
    };
}

#endif
